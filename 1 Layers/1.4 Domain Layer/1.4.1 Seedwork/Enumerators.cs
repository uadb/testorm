﻿
#region Directivas

using System.Runtime.Serialization;

#endregion

namespace Domain.Seedwork.Enumerators
{

    #region Enumeradores Generales

    [DataContract]
    public enum State : int
    {
        [EnumMember]
        None = 0,
        [EnumMember]
        Insert = 1,
        [EnumMember]
        Update = 2,
        [EnumMember]
        Delete = 3,
        [EnumMember]
        Executed = 4,
        [EnumMember]
        Selected = 5,
        [EnumMember]
        Flag1 = 6,
        [EnumMember]
        Flag2 = 7,
        [EnumMember]
        Flag3 = 8
    }

    [DataContract]
    public enum DataType : int
    {
        [EnumMember]
        Cadena = 0,
        [EnumMember]
        Numero = 1,
        [EnumMember]
        Bandera = 2,
        [EnumMember]
        Fecha = 3
    }

    [DataContract]
    public enum MessageType : int
    {
        [EnumMember]
        Informacion = 1,
        [EnumMember]
        Precaucion = 2,
        [EnumMember]
        Error = 3,
        [EnumMember]
        Pregunta = 4
    }

    #endregion

}
