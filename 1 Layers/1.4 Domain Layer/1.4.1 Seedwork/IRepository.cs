﻿#region Directivas

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace Domain.Seedwork
{
    public interface IRepository<TEntity>
        where TEntity : Entity
    {

        #region Metodos

        TEntity SaveChanges(TEntity pItem);
        
        List<TEntity> GetAll();

        #endregion

    }
}
