﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Domain.Seedwork
{
    [DataContract]
    public class Relation : System.Attribute
    {
        public string KeySource { get; set; }
        public string KeyTarget { get; set; }
        public string NameProcedure { get; set; }
    }
}
