﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Domain.Seedwork.Expression
{
    public class ExpressionEntity
    {
        public string Formulation { get; set; }
        public bool Valid { get; set; }
        private Dictionary<string, Object> _parameters = new Dictionary<string, object>();
        public Dictionary<string, Object> Parameters { get { return _parameters; } }
        private static List<Type> lTypeallowed = new List<Type>();

        /// <summary>
        /// Constructor
        /// </summary>
        public ExpressionEntity()
        {
            lTypeallowed.Add(typeof(int));
            lTypeallowed.Add(typeof(int?));
            lTypeallowed.Add(typeof(Int32));
            lTypeallowed.Add(typeof(Int64));
            lTypeallowed.Add(typeof(long));
            lTypeallowed.Add(typeof(String));
            lTypeallowed.Add(typeof(string));
            lTypeallowed.Add(typeof(DateTime));
            lTypeallowed.Add(typeof(DateTime?));
            lTypeallowed.Add(typeof(decimal));
            lTypeallowed.Add(typeof(decimal?));
            lTypeallowed.Add(typeof(Decimal));
            lTypeallowed.Add(typeof(Decimal?));
            lTypeallowed.Add(typeof(double));
            lTypeallowed.Add(typeof(double?));
            lTypeallowed.Add(typeof(Double));
        }

        /// <summary>
        /// Operador equivalenter a = en  ANSISQL
        /// </summary>
        /// <typeparam name="T">Tipo de valor a comparar</typeparam>
        /// <param name="valueleft">Arbol de expresiones lambda</param>
        /// <param name="valueRigth">Valor a comparar</param>
        /// <returns>ExpressionEntity de la evaluacion</returns>
        public static ExpressionEntity Equal<T>(Expression<Func<T, T>> valueleft, T valueRigth)
        {
            ExpressionEntity vResul = new ExpressionEntity();
            if (valueRigth != null && !lTypeallowed.Contains(typeof(T)))
            {
                vResul.Formulation = string.Empty;
                vResul.Valid = false;
            }
            string vNameVariable = ((MemberExpression)valueleft.Body).Member.Name;
            if (valueRigth != null)
            {
                string parameterName = addParameter(vResul._parameters, valueRigth);
                vResul.Formulation = String.Format("{0} = {1}", vNameVariable, parameterName);
            }
            else
            {
                vResul.Formulation = String.Format("{0} IS NULL", vNameVariable);
            }

            vResul.Valid = true;
            return vResul;
        }

        /// <summary>
        /// Operador equivalenter a < en  ANSISQL
        /// </summary>
        /// <typeparam name="T">Tipo de valor a comparar</typeparam>
        /// <param name="valueleft">Arbol de expresiones lambda</param>
        /// <param name="valueRigth">Valor a comparar</param>
        /// <returns>ExpressionEntity de la evaluacion</returns>
        public static ExpressionEntity Less<T>(Expression<Func<T, T>> valueleft, T valueRigth)
        {
            ExpressionEntity vResul = new ExpressionEntity();
            if (valueRigth != null && !lTypeallowed.Contains(typeof(T)))
            {
                vResul.Formulation = string.Empty;
                vResul.Valid = false;
            }
            string vNameVariable = ((MemberExpression)valueleft.Body).Member.Name;
            string parameterName = addParameter(vResul._parameters, valueRigth);
            vResul.Formulation = String.Format("{0} < {1}", vNameVariable, parameterName);
            vResul.Valid = true;
            return vResul;
        }

        /// <summary>
        /// Operador equivalenter a <= en  ANSISQL
        /// </summary>
        /// <typeparam name="T">Tipo de valor a comparar</typeparam>
        /// <param name="valueleft">Arbol de expresiones lambda</param>
        /// <param name="valueRigth">Valor a comparar</param>
        /// <returns>ExpressionEntity de la evaluacion</returns>
        public static ExpressionEntity LessOrEqual<T>(Expression<Func<T, T>> valueleft, T valueRigth)
        {
            ExpressionEntity vResul = new ExpressionEntity();
            if (valueRigth != null && !lTypeallowed.Contains(typeof(T)))
            {
                vResul.Formulation = string.Empty;
                vResul.Valid = false;
            }
            string vNameVariable = ((MemberExpression)valueleft.Body).Member.Name;
            string parameterName = addParameter(vResul._parameters, valueRigth);
            vResul.Formulation = String.Format("{0} <= {1}", vNameVariable, parameterName);
            vResul.Valid = true;
            return vResul;
        }

        /// <summary>
        /// Operador equivalenter a > en  ANSISQL
        /// </summary>
        /// <typeparam name="T">Tipo de valor a comparar</typeparam>
        /// <param name="valueleft">Arbol de expresiones lambda</param>
        /// <param name="valueRigth">Valor a comparar</param>
        /// <returns>ExpressionEntity de la evaluacion</returns>
        public static ExpressionEntity Higher<T>(Expression<Func<T, T>> valueleft, T valueRigth)
        {
            ExpressionEntity vResul = new ExpressionEntity();
            if (valueRigth != null && !lTypeallowed.Contains(typeof(T)))
            {
                vResul.Formulation = string.Empty;
                vResul.Valid = false;
            }
            string vNameVariable = ((MemberExpression)valueleft.Body).Member.Name;
            string parameterName = addParameter(vResul._parameters, valueRigth);
            vResul.Formulation = String.Format("{0} > {1}", vNameVariable, parameterName);
            vResul.Valid = true;
            return vResul;
        }

        /// <summary>
        /// Operador equivalenter a >= en  ANSISQL
        /// </summary>
        /// <typeparam name="T">Tipo de valor a comparar</typeparam>
        /// <param name="valueleft">Arbol de expresiones lambda</param>
        /// <param name="valueRigth">Valor a comparar</param>
        /// <returns>ExpressionEntity de la evaluacion</returns>
        public static ExpressionEntity HigherOrEqual<T>(Expression<Func<T, T>> valueleft, T valueRigth)
        {
            ExpressionEntity vResul = new ExpressionEntity();
            if (valueRigth != null && !lTypeallowed.Contains(typeof(T)))
            {
                vResul.Formulation = string.Empty;
                vResul.Valid = false;
            }
            string vNameVariable = ((MemberExpression)valueleft.Body).Member.Name;
            string parameterName = addParameter(vResul._parameters, valueRigth);
            vResul.Formulation = String.Format("{0} >= {1}", vNameVariable, parameterName);
            vResul.Valid = true;
            return vResul;
        }

        /// <summary>
        /// Operador equivalenter a <> en  ANSISQL
        /// </summary>
        /// <typeparam name="T">Tipo de valor a comparar</typeparam>
        /// <param name="valueleft">Arbol de expresiones lambda</param>
        /// <param name="valueRigth">Valor a comparar</param>
        /// <returns>ExpressionEntity de la evaluacion</returns>
        public static ExpressionEntity Distinct<T>(Expression<Func<T, T>> valueleft, T valueRigth)
        {
            ExpressionEntity vResul = new ExpressionEntity();
            if (valueRigth != null && !lTypeallowed.Contains(typeof(T)))
            {
                ;
                vResul.Formulation = string.Empty;
                vResul.Valid = false;
            }
            string vNameVariable = ((MemberExpression)valueleft.Body).Member.Name;

            if (valueRigth != null)
            {
                string parameterName = addParameter(vResul._parameters, valueRigth);
                vResul.Formulation = String.Format("{0} <> {1}", vNameVariable, parameterName);
            }
            else
            {
                vResul.Formulation = String.Format("{0} IS NOT NULL", vNameVariable);
            }

            vResul.Valid = true;
            return vResul;
        }

        /// <summary>
        /// Operador equivalenter a like en  ANSISQL
        /// </summary>
        /// <typeparam name="T">Tipo de valor a comparar</typeparam>
        /// <param name="valueleft">Arbol de expresiones lambda</param>
        /// <param name="valueRigth">Valor a comparar</param>
        /// <returns>ExpressionEntity de la evaluacion</returns>
        public static ExpressionEntity Like<T>(Expression<Func<T, T>> valueleft, T valueRigth)
        {
            ExpressionEntity vResul = new ExpressionEntity();
            if (valueRigth != null && !lTypeallowed.Contains(typeof(T)))
            {
                vResul.Formulation = string.Empty;
                vResul.Valid = false;
            }
            string vNameVariable = ((MemberExpression)valueleft.Body).Member.Name;
            string parameterName = addParameter(vResul._parameters, valueRigth);
            vResul.Formulation = String.Format("COALESCE({0},' ') LIKE {1}", vNameVariable, parameterName);
            vResul.Valid = true;
            return vResul;
        }


        /// <summary>
        /// Operador equivalenter a In en  ANSISQL
        /// </summary>
        /// <typeparam name="T">Tipo de valor a comparar</typeparam>
        /// <param name="valueleft">Arbol de expresiones lambda</param>
        /// <param name="valueRigth">Valor a comparar</param>
        /// <returns>ExpressionEntity de la evaluacion</returns>
        public static ExpressionEntity In<T>(Expression<Func<T, T>> valueleft, List<T> valueRigth)
        {
            ExpressionEntity vResul = new ExpressionEntity();
            if (valueRigth != null && !lTypeallowed.Contains(typeof(T)))
            {
                vResul.Formulation = string.Empty;
                vResul.Valid = false;
            }
            string vNameVariable = ((MemberExpression)valueleft.Body).Member.Name;
            string inFormulation = "( ";
            foreach (var item in valueRigth)
            {
                inFormulation += (valueRigth.IndexOf(item) == 0 ? "" : ",") + addParameter(vResul._parameters, item);
            }
            inFormulation += ")";
            vResul.Formulation = String.Format("{0} IN {1}", vNameVariable, inFormulation);
            vResul.Valid = true;
            return vResul;
        }

        /// <summary>
        /// Operador equivalenter a and en  ANSISQL
        /// </summary>
        /// <typeparam name="T">Tipo de valor a comparar</typeparam>
        /// <param name="valueleft">Arbol de expresiones lambda</param>
        /// <param name="valueRigth">Valor a comparar</param>
        /// <returns>ExpressionEntity de la evaluacion</returns>
        public ExpressionEntity AND(ExpressionEntity ExpressionIn)
        {
            ExpressionEntity vResul = new ExpressionEntity();
            if (!ExpressionIn.Valid)
            {
                throw new Exception("Expression no valid");
            }
            var KeyThis = from q in Parameters select new { key = q.Key, value = q.Value };
            KeyThis.ToList().ForEach(x => addParameter(vResul.Parameters, x.value));
            var KeyIn = from q in ExpressionIn.Parameters select new { key = q.Key, value = q.Value };
            KeyIn.ToList().ForEach(x => addParameter(vResul.Parameters, x.value));

            vResul.Formulation = String.Format("{0} AND {1}", Formulation, ExpressionIn.Formulation);
            vResul.Valid = true;

            return vResul;
        }

        /// <summary>
        /// Operador equivalenter a OR en  ANSISQL
        /// </summary>
        /// <typeparam name="T">Tipo de valor a comparar</typeparam>
        /// <param name="valueleft">Arbol de expresiones lambda</param>
        /// <param name="valueRigth">Valor a comparar</param>
        /// <returns>ExpressionEntity de la evaluacion</returns>
        public ExpressionEntity OR(ExpressionEntity ExpressionIn)
        {
            ExpressionEntity vResul = new ExpressionEntity();
            if (!ExpressionIn.Valid)
            {
                throw new Exception("Expression no valid");
            }
            var KeyThis = from q in Parameters select new { key = q.Key, value = q.Value };
            KeyThis.ToList().ForEach(x => addParameter(vResul.Parameters, x.value));
            var KeyIn = from q in ExpressionIn.Parameters select new { key = q.Key, value = q.Value };
            KeyIn.ToList().ForEach(x => addParameter(vResul.Parameters, x.value));

            vResul.Formulation = String.Format("{0} OR {1}", Formulation, ExpressionIn.Formulation);
            vResul.Valid = true;

            return vResul;
        }

        /// <summary>
        /// Operador equivalenter a Order en  ANSISQL
        /// </summary>
        /// <typeparam name="T">Tipo de valor a comparar</typeparam>
        /// <param name="valueleft">Arbol de expresiones lambda</param>
        /// <param name="valueRigth">Valor a comparar</param>
        /// <returns>ExpressionEntity de la evaluacion</returns>
        public ExpressionEntity OrderBy<T>(Expression<Func<T, T>> valueleft, bool AscDesc)
        {
            ExpressionEntity vResul = this;
            string AscDescendent = AscDesc == true ? "ASC" : "DESC";
            
            string vNameVariable = ((MemberExpression)valueleft.Body).Member.Name;

            vResul.Formulation += String.Format(" ORDER BY {0} {1}", vNameVariable, AscDescendent);
            vResul.Valid = true;
            return vResul;
        }


        /// <summary>
        /// Adiciona un parametro al array de parametros
        /// </summary>
        /// <param name="parameters">Parametros de la expression</param>
        /// <param name="value">valor a asignar al arreglo</param>
        /// <returns></returns>
        private static string addParameter(Dictionary<string, object> parameters, object value)
        {
            string vNameParameter = ":parameter" + parameters.Count;
            parameters.Add(vNameParameter, value);
            vNameParameter = ":parameter_#####";
            return vNameParameter;
        }

        /// <summary>
        /// metodo que reformula la expresion
        /// </summary>
        /// <param name="pExpresion">Expression a reformular</param>
        public static void ReFormulation(ExpressionEntity pExpresion)
        {
            List<string> vTemp = pExpresion.Formulation.Split(new string[] { "_#####" }, StringSplitOptions.None).ToList();
            pExpresion.Formulation = "";
            int cont = 0;
            foreach (var item in vTemp)
            {
                if (item.Contains(":parameter"))
                {
                    pExpresion.Formulation += item + cont;// vTemp.IndexOf(item);
                    cont++;
                }
                else
                {
                    pExpresion.Formulation += item;
                }
            }

        }
    }
}
