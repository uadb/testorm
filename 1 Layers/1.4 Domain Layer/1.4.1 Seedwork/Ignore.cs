using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Domain.Seedwork
{
    [DataContract]
    public class Ignore : System.Attribute
    {
        private bool insert = true;

        public bool Insert
        {
            get { return insert; }
            set { insert = value; }
        }

        private bool update = true;

        public bool Update
        {
            get { return update; }
            set { update = value; }
        }

        private bool delete = true;

        public bool Delete
        {
            get { return delete; }
            set { delete = value; }
        }
    }
}
