﻿using Npgsql;
using NpgsqlTypes;
using System;


namespace Domain.Seedwork
{
    
    public class ParamOut
    {
        public enum Type
        {
            Out,
            InOut,
            In
        }

        private Object _value;
        public Object Valor { get { return _value; } set { _value = value; } }
        public Type InOut { get; set; }
        public Boolean IsObjectOracle { get; set; }

        public int Size { get; set; }

        public ParamOut(Object value)
        {
            _value = value;
        }

        public NpgsqlParameter  BindObjecPostGrest( string parameName )
        {
            NpgsqlParameter pram = new NpgsqlParameter();
            pram.ParameterName = parameName;
            pram.NpgsqlDbType = NpgsqlDbType.Json;
            pram.NpgsqlValue = Valor.GetType().Name;
          
            switch (InOut)
            {
                case Type.Out: 
                    pram.Direction = System.Data.ParameterDirection.Output;
                    break;
                case Type.InOut:
                    pram.Direction = System.Data.ParameterDirection.InputOutput;
                    break;
                case Type.In:
                    pram.Direction = System.Data.ParameterDirection.Input;
                    break;
                default:
                    pram.Direction = System.Data.ParameterDirection.Input;
                    break;
            }

            pram.Value = Valor;
            return pram;            
        }

    }
}
