﻿using System.Runtime.Serialization;

namespace Domain.Seedwork
{
    [DataContract]
    public class Key : System.Attribute
    {
        private bool _Autogenerate = false;
        public bool Autogenerate
        {
            get { return _Autogenerate; }
            set { _Autogenerate = value; }
        }

        private string _SequenceName = string.Empty;
        public string SequenceName
        {
            get { return _SequenceName; }
            set { _SequenceName = value; }
        }
    }
}
