﻿#region Direcitvas

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.IO;
using System.Web;
#endregion

namespace Domain.Seedwork.Validation
{
    [Serializable]
    public class Validator : System.Attribute
    {
        #region Clases
         [Serializable]
        public class RankDate
        {
            public RankDate(DateTime minDate, DateTime maxDate)
            {
                this.MinDate = minDate;
                this.MaxDate = maxDate;
                
            }

            public DateTime MinDate { get; set; }
            public DateTime MaxDate { get; set; }
        }

         [Serializable]
        public class RankNumber
        {
            public RankNumber(Decimal minValue, Decimal maxValue)
            {
                this.MinValue = minValue;
                this.MaxValue = maxValue;
            }

            public Decimal MinValue { get; set; }
            public Decimal MaxValue { get; set; }
        }

         [Serializable]
        public class ResultValidationField
        {
            public string Condition { get; set; }
            public bool Valid { get; set; }
        }


        #endregion

        #region Constructores

        public Validator()
        {

        }

        public Validator(RankDate rangoFechas)
        {
            RankDates = rangoFechas;
        }


        public Validator(RankNumber rangoNumero)
        {
            RankNumbers = rangoNumero;
        }
        #endregion

        #region Propiedades
        public bool IsRequired { get; set; }
        private int _maxLength = 0;
        public int MaxLength { get { return _maxLength; } set { _maxLength = value; } }
        public bool IsNegative { get; set; }
        public RankDate RankDates { get; set; }
        public RankNumber RankNumbers { get; set; }
        public string RegularExpression { get; set; }
        private double _minValueNumber = double.MinValue;
        public double MinValueNumber { get; set; }
        public string NameDisplay { get; set; }
        public string RegularExpressionType { get; set; }
        public string RegularCustomMessage { get; set; }
        private bool isDateTimeNow = false;
        public bool IsDateTimeNow { get { return isDateTimeNow; } set { isDateTimeNow = value; } }
        #endregion

        /// <summary>
        /// Realiza la validacion de los POCOS a travez de un decorado
        /// </summary>
        /// <param name="pEntidad">Entidad a validar</param>
        /// <param name="pResultValidacion"> Indica si logro validar correctamente</param>
        /// <param name="pMenssageFormat">Mensaje con los errores producto de la validacion</param>
        /// <returns>DIccionario con los mensajes</returns>
        public static Dictionary<string, ResultValidationField> ValidateFromAtributte(Entity pEntidad, ref bool pResultValidacion, ref string pMenssageFormat)
        {
            Dictionary<string, ResultValidationField> vResult = new Dictionary<string, ResultValidationField>();
            XmlDocument documentResul = new XmlDocument();
            documentResul.LoadXml(String.Format("<ValidationMessageResul Object = '{0}'></ValidationMessageResul>", pEntidad.GetType().Name.Replace(".", "_")));

            foreach (PropertyInfo item in pEntidad.GetType().GetProperties())
            {
                XmlNode validateField = documentResul.CreateNode(XmlNodeType.Element, "Field", null);
                XmlAttribute nameField = documentResul.CreateAttribute("nameField");
                XmlAttribute MessageField = documentResul.CreateAttribute("MessageField");
                XmlAttribute IsValidate = documentResul.CreateAttribute("IsValidate");

                ResultValidationField resultValidationField = new ResultValidationField();
                validateField.Attributes.Append(nameField);
                validateField.Attributes.Append(MessageField);
                validateField.Attributes.Append(IsValidate);
                IsValidate.Value = "true";

                nameField.Value = item.Name;
                resultValidationField.Condition = "";
                resultValidationField.Valid = true;

                Type tipo = item.GetType();
                Ignore atributoIgnore = Attribute.GetCustomAttributes(item).ToList<System.Attribute>().Where(a => a.GetType() == typeof(Ignore)).FirstOrDefault() as Ignore;

                if (atributoIgnore != null)
                {
                    continue;
                }

                Validator atributo = Attribute.GetCustomAttributes(item).ToList<System.Attribute>().Where(a => a.GetType() == typeof(Validator)).FirstOrDefault() as Validator;

                if (atributo == null)
                {
                    MessageField.Value = "PROPIEDAD NO MARCADA PARA VALIDAR";
                    resultValidationField.Valid = true;
                    IsValidate.Value = "false";
                    vResult.Add(item.Name, resultValidationField);
                    documentResul.DocumentElement.AppendChild(validateField);
                    continue;
                }

                if (atributo.IsRequired)
                {
                    if (item.GetValue(pEntidad, null) == null || item.GetValue(pEntidad, null).ToString().Equals("") || item.GetValue(pEntidad, null).ToString().Equals("-1"))
                    {
                        MessageField.Value = atributo.NameDisplay + " ES UN VALOR REQUERIDO";
                        resultValidationField.Valid = false;
                        resultValidationField.Condition += "| IsRequired";
                    }
                }

                if (atributo.MaxLength > 0 && item.GetValue(pEntidad, null) != null)
                {
                    if (item.GetValue(pEntidad, null).ToString().Length > atributo.MaxLength)
                    {
                        MessageField.Value += atributo.NameDisplay + String.Format(" DEBE TENER {0} CARACTERE(S)", atributo.MaxLength);
                        resultValidationField.Valid = false;
                        resultValidationField.Condition += "| MaxLength";
                    }
                }

                #region Adicion de tipos numeros
                List<Type> lTipos = new List<Type>();
                lTipos.Add(typeof(decimal));
                lTipos.Add(typeof(Decimal));
                lTipos.Add(typeof(int));
                lTipos.Add(typeof(Int16));
                lTipos.Add(typeof(Int32));
                lTipos.Add(typeof(Int64));
                lTipos.Add(typeof(float));
                lTipos.Add(typeof(double));
                lTipos.Add(typeof(Double));
                #endregion

                if (!atributo.IsNegative && pEntidad != null && item.GetValue(pEntidad, null) != null && lTipos.Contains(item.GetValue(pEntidad, null).GetType()))
                {
                    if (Convert.ToDouble(item.GetValue(pEntidad, null)) < 0)
                    {
                        MessageField.Value += atributo.NameDisplay + " NO PUEDE SER NEGATIVO";
                        resultValidationField.Valid = false;
                        resultValidationField.Condition += "| IsNegative";
                    }
                }

                if (atributo.MinValueNumber != double.MinValue && pEntidad != null && item.GetValue(pEntidad, null) != null && lTipos.Contains(item.GetValue(pEntidad, null).GetType()))
                {
                    if (Convert.ToDouble(item.GetValue(pEntidad, null)) < atributo.MinValueNumber)
                    {
                        MessageField.Value += atributo.NameDisplay + String.Format(" NO PUEDE SER MENOR A: {0}", Convert.ToString(atributo.MinValueNumber.ToString()));
                        resultValidationField.Valid = false;
                        resultValidationField.Condition += "| IsNegative";
                    }
                }

                if (atributo.RegularExpression != null
                    && !atributo.RegularExpression.Equals("")
                    && pEntidad != null
                    && item.GetValue(pEntidad, null).GetType() == typeof(System.String)
                    )
                {
                    Regex rx = new Regex(atributo.RegularExpression);
                    if (!rx.IsMatch(Convert.ToString(item.GetValue(pEntidad, null))))
                    {
                        if (!string.IsNullOrEmpty(atributo.RegularCustomMessage))
                        {
                            MessageField.Value += String.Format("{0} : {1}", atributo.NameDisplay, atributo.RegularCustomMessage);
                            resultValidationField.Valid = false;
                            resultValidationField.Condition += "| IsNegative";
                        }
                        else
                        {
                            MessageField.Value += atributo.RegularExpressionType == "" ? atributo.NameDisplay + " NO CUMPLE CON LA EXPRESION REGULAR" : String.Format("{0}. {1}", atributo.NameDisplay, atributo.RegularExpressionType);
                            resultValidationField.Valid = false;
                            resultValidationField.Condition += "| IsNegative";
                        }
                        
                    }
                }

                List<Type> lTiposDate = new List<Type>();
                lTiposDate.Add(typeof(DateTime));
                lTiposDate.Add(typeof(DateTime?));


                if (atributo.isDateTimeNow && pEntidad != null &&  lTiposDate.Contains(item.GetValue(pEntidad, null).GetType()))
                {
                    if (item.GetValue(pEntidad, null) == null || Convert.ToDateTime(item.GetValue(pEntidad, null)) < DateTime.Parse(DateTime.Now.ToShortDateString()))
                    {
                        MessageField.Value += atributo.NameDisplay + String.Format(" NO PUEDE SER MENOR A: {0}", DateTime.Now.ToShortDateString());
                        resultValidationField.Valid = false;
                        resultValidationField.Condition += "| IsNegative";
                    }
                }


                if (!resultValidationField.Valid)
                {
                    pResultValidacion = !resultValidationField.Valid;
                }

                vResult.Add(item.Name, resultValidationField);
                documentResul.DocumentElement.AppendChild(validateField);
            }
            pMenssageFormat = documentResul.OuterXml;
            return vResult;
        }

        /// <summary>
        /// Obtiene codigo HTML a travez del xml generado del componente de la validacion
        /// </summary>
        /// <param name="xml">estructura xml producto del componente de validacion</param>
        /// <returns></returns>
        public static String getMenssageHTML(string xml)
        {
            XDocument document = XDocument.Parse(xml);
            StringWriter myWriter = new StringWriter();
            String html = "<ul class = 'validationwarning'>";

            (from x in document.Elements("ValidationMessageResul").Elements("Field") 
             where x.Attribute("IsValidate").Value == "true" && !x.Attribute("MessageField").Value.Equals("") select x).ToList().
             ForEach(a => html += "<li><span class = 'labelWarning'>" + char.ToUpper(a.Attribute("MessageField").Value[0]) + a.Attribute("MessageField").Value.Substring(1).ToLower() + "</span></li>");

            html += "</ul>";

            HttpUtility.HtmlDecode(html, myWriter);

            return myWriter.ToString();
        }

        /// <summary>
        /// Realiza la validacion a partir de la propiedad PropertysValidator del POCO
        /// </summary>
        /// <param name="pEntidad">Entidad a validar</param>
        /// <param name="pResultValidacion"> Indica si logro validar correctamente</param>
        /// <param name="pMenssageFormat">Mensaje con los errores producto de la validacion</param>
        /// <returns>DIccionario con los mensajes</returns>
        public static Dictionary<string, ResultValidationField> Validate(Entity pEntidad, ref bool pResultValidacion, ref string pMenssageFormat)
        {
            Dictionary<string, ResultValidationField> vResult = new Dictionary<string, ResultValidationField>();
            XmlDocument documentResul = new XmlDocument();
            documentResul.LoadXml(String.Format("<ValidationMessageResul Object = '{0}'></ValidationMessageResul>", pEntidad.GetType().Name.Replace(".", "_")));

            foreach (PropertyInfo item in pEntidad.PropertysValidator.Keys)
            {
                if (item == null)
                {
                    throw new Exception("Propiedad inexistente para validacion");
                }
                XmlNode validateField = documentResul.CreateNode(XmlNodeType.Element, "Field", null);
                XmlAttribute nameField = documentResul.CreateAttribute("nameField");
                XmlAttribute MessageField = documentResul.CreateAttribute("MessageField");
                XmlAttribute IsValidate = documentResul.CreateAttribute("IsValidate");
                
                ResultValidationField resultValidationField = new ResultValidationField();
                validateField.Attributes.Append(nameField);
                validateField.Attributes.Append(MessageField);
                validateField.Attributes.Append(IsValidate);
                IsValidate.Value = "true";

                nameField.Value = item.Name;
                resultValidationField.Condition = "";
                resultValidationField.Valid = true;

                Type tipo = item.GetType();
                //Validator atributo = Attribute.GetCustomAttributes(item).ToList<System.Attribute>().Where(a => a.GetType() == typeof(Validator)).FirstOrDefault() as Validator;
                Validator atributo = pEntidad.PropertysValidator[item];
                Ignore atributoIgnore = Attribute.GetCustomAttributes(item).ToList<System.Attribute>().Where(a => a.GetType() == typeof(Ignore)).FirstOrDefault() as Ignore;

                if (atributoIgnore != null)
                {
                    continue;
                }
                if (atributo == null)
                {
                    MessageField.Value = "PROPIEDAD NO MARCADA PARA VALIDAR";
                    resultValidationField.Valid = true;
                    IsValidate.Value = "false";
                    vResult.Add(item.Name, resultValidationField);
                    documentResul.DocumentElement.AppendChild(validateField);
                    continue;
                }

                if (atributo.IsRequired)
                {
                    if (item.GetValue(pEntidad, null) == null || item.GetValue(pEntidad, null).ToString().Equals(""))
                    {
                        MessageField.Value = atributo.NameDisplay + " ES UN VALOR REQUERIDO " +  atributo.RegularCustomMessage;
                        resultValidationField.Valid = false;
                        resultValidationField.Condition += "| IsRequired";
                    }
                }

                if (atributo.MaxLength > 0 && item.GetValue(pEntidad, null) != null)
                {
                    if (item.GetValue(pEntidad, null).ToString().Length > atributo.MaxLength && !item.PropertyType.FullName.Contains("DateTime"))
                    {
                        MessageField.Value += atributo.NameDisplay + String.Format(" DEBE TENER {0} CARACTERE(S)", atributo.MaxLength);
                        resultValidationField.Valid = false;
                        resultValidationField.Condition += "| MaxLength";
                    }
                }

                #region Adicion de tipos numeros
                List<Type> lTipos = new List<Type>();
                lTipos.Add(typeof(decimal));
                lTipos.Add(typeof(Decimal));
                lTipos.Add(typeof(int));
                lTipos.Add(typeof(Int16));
                lTipos.Add(typeof(Int32));
                lTipos.Add(typeof(Int64));
                lTipos.Add(typeof(float));
                lTipos.Add(typeof(double));
                lTipos.Add(typeof(Double));
                #endregion

                if (!atributo.IsNegative && pEntidad != null && item.GetValue(pEntidad, null) != null && lTipos.Contains(item.GetValue(pEntidad, null).GetType()))
                {
                    if (Convert.ToDouble(item.GetValue(pEntidad, null)) < 0)
                    {
                        MessageField.Value += atributo.NameDisplay + " NO PUEDE SER NEGATIVO ";
                        resultValidationField.Valid = false;
                        resultValidationField.Condition += "| IsNegative";
                    }
                }

                if (atributo.MinValueNumber != double.MinValue && pEntidad != null && item.GetValue(pEntidad, null) != null && lTipos.Contains(item.GetValue(pEntidad, null).GetType()))
                {
                    if (Convert.ToDouble(item.GetValue(pEntidad, null)) < atributo.MinValueNumber)
                    {
                        MessageField.Value += atributo.NameDisplay + String.Format(" NO PUEDE SER MENOR A: {0}", Convert.ToString(atributo.MinValueNumber.ToString()));
                        resultValidationField.Valid = false;
                        resultValidationField.Condition += "| IsNegative";
                    }
                }

                if (atributo.RegularExpression != null
                    && item.GetValue(pEntidad, null) != null
                    && !atributo.RegularExpression.Equals("")
                    && pEntidad != null
                    && item.GetValue(pEntidad, null).GetType() == typeof(System.String)
                    )
                {
                    Regex rx = new Regex(atributo.RegularExpression);
                    if (!rx.IsMatch(Convert.ToString(item.GetValue(pEntidad, null))))
                    {
                        if (!string.IsNullOrEmpty(atributo.RegularCustomMessage))
                        {
                            MessageField.Value += String.Format("{0} : {1}", atributo.NameDisplay, atributo.RegularCustomMessage);
                            resultValidationField.Valid = false;
                            resultValidationField.Condition += "| IsNegative";
                        }
                        else
                        {
                            MessageField.Value += atributo.RegularExpressionType == "" ? atributo.NameDisplay + " NO CUMPLE CON LA EXPRESION REGULAR" : String.Format("{0}. {1}", atributo.NameDisplay, atributo.RegularExpressionType);
                            resultValidationField.Valid = false;
                            resultValidationField.Condition += "| IsNegative";
                        }
                        
                    }
                }

                List<Type> lTiposDate = new List<Type>();
                lTiposDate.Add(typeof(DateTime));
                lTiposDate.Add(typeof(DateTime?));


                if (atributo.isDateTimeNow && pEntidad != null && lTiposDate.Contains(item.GetValue(pEntidad, null).GetType()))
                {
                    if (item.GetValue(pEntidad, null) == null || Convert.ToDateTime(item.GetValue(pEntidad, null)) < DateTime.Parse(DateTime.Now.ToShortDateString()))
                    {
                        MessageField.Value += atributo.NameDisplay + String.Format(" NO PUEDE SER MENOR A: {0}", DateTime.Now.ToShortDateString());
                        resultValidationField.Valid = false;
                        resultValidationField.Condition += "| IsNegative";
                    }
                }


                if (atributo.RankDates != null && pEntidad != null && lTiposDate.Contains(item.GetValue(pEntidad, null).GetType()))
                {
                    if (item.GetValue(pEntidad, null) == null ||
                        !(Convert.ToDateTime(item.GetValue(pEntidad, null)) <= atributo.RankDates.MaxDate && Convert.ToDateTime(item.GetValue(pEntidad, null)) >= atributo.RankDates.MinDate)
                        )
                    {
                        MessageField.Value += atributo.NameDisplay + String.Format(" DE ESTAR ENTRE {0} Y {1}", atributo.RankDates.MaxDate.ToShortDateString(), atributo.RankDates.MaxDate.ToShortDateString());
                        resultValidationField.Valid = false;
                        resultValidationField.Condition += "| IsNegative";
                    }
                }


                if (!resultValidationField.Valid)
                {
                    pResultValidacion = !resultValidationField.Valid;
                }

                vResult.Add(item.Name, resultValidationField);
                documentResul.DocumentElement.AppendChild(validateField);
            }
            pMenssageFormat = documentResul.OuterXml;
            return vResult;
        }


        /// <summary>
        /// Genera Mensaje formateado lista c# a una lista html
        /// </summary>
        /// <param name="vMensaje">Lista de string 'Mensajes' </param>
        /// <returns>Un string para el cliente formateado en listas html</returns>
        public static String _GetListHTML(List<string> vMensaje)
        {
            StringBuilder vMensajeFinal = new StringBuilder();
            vMensajeFinal.Append("<ul class = 'validationwarning'>");
            foreach (string vItem in vMensaje)
            {
                vMensajeFinal.Append("<li><span class = 'labelWarning'>");
                vMensajeFinal.Append(vItem);
                vMensajeFinal.Append("</span></li>");
            }
            vMensajeFinal.Append("</ul>");
            return vMensajeFinal.ToString();
        }
    
    }
}

