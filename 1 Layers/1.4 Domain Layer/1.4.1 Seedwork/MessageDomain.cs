﻿
#region Directivas

using System.Runtime.Serialization;

#endregion
namespace Domain.Seedwork
{
    [DataContract]
    public class MessageDomain
    {

        #region Propiedades

        /// <summary>
        /// Identificador del mensaje.
        /// </summary>
        [DataMember]
        public int IDMessage { get; set; }

        /// <summary>
        /// Tipo de mensaje.
        /// </summary>
        [DataMember]
        public Domain.Seedwork.Enumerators.MessageType MessageType { get; set; }

        /// <summary>
        /// Contenido del mensaje.
        /// </summary>
        [DataMember]
        public string Content { get; set; }

        #endregion

    }
}
