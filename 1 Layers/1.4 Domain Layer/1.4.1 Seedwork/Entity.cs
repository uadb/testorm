﻿#region Directivas

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Reflection;
using Domain.Seedwork.Validation;

#endregion

namespace Domain.Seedwork
{
    [Serializable]
    [DataContract(IsReference = true)]
    public class Entity
    {

        #region Propiedades

        /// <summary>
        /// Estado de la entidad.
        /// </summary>
        [DataMember]
        public Enumerators.State State { get; set; }

        [DataMember]
        public string UnidadEjecutora { get; set; }

        [DataMember]
        public string Institucion { get; set; }

        [DataMember]
        public string Modulo { get; set; }

        [DataMember]
        public string Sistema { get; set; }

        [DataMember]
        public string Gestion { get; set; }

        [DataMember]
        public string Usuario { get; set; }

        [DataMember]
        public string Url { get; set; }

        #endregion
        /// <summary>
        /// Diccionario de validacion de propiedades, mismo que no debe tener 
        /// el atributo DataContract para usar los POCO en servicios
        /// </summary>
        [System.Xml.Serialization.XmlIgnoreAttribute]
        public Dictionary<PropertyInfo, Validator> PropertysValidator = new Dictionary<PropertyInfo, Validator>();

        public Entity()
        {
            State = Enumerators.State.None;
            Usuario = null;
            UnidadEjecutora = null;

            if (System.Web.HttpContext.Current != null)
            {
                if (System.Web.HttpContext.Current.Session != null)
                {
                    Usuario = System.Web.HttpContext.Current.Session["USUARIO"] == null ? null : System.Web.HttpContext.Current.Session["USUARIO"].ToString();
                    UnidadEjecutora = System.Web.HttpContext.Current.Session["ENTIDAD"] == null ? null : System.Web.HttpContext.Current.Session["ENTIDAD"].ToString();
                }
            }
        }

    }
}
