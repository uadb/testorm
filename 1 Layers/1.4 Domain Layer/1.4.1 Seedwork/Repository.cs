﻿#region Directivas

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.Reflection;

using System.Collections;
using Domain.Seedwork.Expression;
using Domain.Seedwork.Resources;
using Npgsql;
using NpgsqlTypes;

#endregion

namespace Domain.Seedwork
{
    /// <summary>
    /// Super Layered Type para la capa de Infrastructura.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class Repository<TEntity> : IRepository<TEntity>
        where TEntity : Entity, new()
    {

        #region Variables
        protected NpgsqlCommand pgCommand;
        protected NpgsqlCommand pgCommandForTransaction;
        protected NpgsqlCommandBuilder pgCommandBuilder;
        protected NpgsqlCommandBuilder pgCommandBuilderForTransaction;

        protected NpgsqlConnection pgConnection;
        protected NpgsqlConnection pgConnectionForTransaction;
        protected NpgsqlConnectionStringBuilder pgConnectionStringBuilder;
        protected NpgsqlConnectionStringBuilder pgConnectionStringBuilderForTransaction;

        public NpgsqlTransaction pgTransaction;

        private string gMessage;

        #endregion

        #region Propiedades

        public string Message
        {
            get
            {
                return gMessage;
            }
            set
            {
                gMessage = value;
            }
        }

        public String Schema
        {
            get
            {
                string schema = "";
                if (pgConnection != null && !String.IsNullOrEmpty(pgConnection.ConnectionString))
                {
                    schema = (from q in pgConnection.ConnectionString.Split(';') where q.StartsWith("User") select q).First().Split('=')[1].Trim();
                }
                return schema;
            }
        }

        public string ConexionString { get; set; }

        /// <summary>
        /// Delegado para inyectar un metodo a SaveChanges
        /// </summary>
        /// <param name="ObjEntity">Entity a ingresar</param>
        public delegate void LoadObject(Object ObjEntity);

        /// <summary>
        /// Delegado para inyectar un metodo a SaveChanges
        /// </summary>
        /// <param name="ObjEntity">Entity a ingresar</param>
        public delegate void Commit();

        public LoadObject HandlerSaveChange { get; set; }
        public Commit HandlerCommite { get; set; }
        
        #endregion

        #region Metodos Base

        /// <summary>
        /// Abre la conexion
        /// </summary>
        protected void OpenConnection()
        {
            try
            {
                if (pgConnection != null)
                {
                    lock (pgConnection)
                    {
                        if (pgConnection.State == System.Data.ConnectionState.Closed)
                        {
                            pgConnection.Open();
                            //pgConnection.SetSessionInfo(gGlobalization);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Abre conexion para una transaccion.
        /// </summary>
        protected void OpenConnectionForTransaction()
        {
            try
            {
                if (pgConnectionForTransaction != null)
                {
                    lock (pgConnectionForTransaction)
                    {
                        if (pgConnectionForTransaction.State == System.Data.ConnectionState.Closed)
                        {
                            pgConnectionForTransaction.Open();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Libera recursos de la conexión.
        /// </summary>
        protected void DisposeConnection()
        {
            try
            {
                if (pgConnection != null)
                {
                    if (pgConnection.State != System.Data.ConnectionState.Closed)
                    {
                        pgConnection.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Libera recursos de la conexión.
        /// </summary>
        protected void DisposeConnectionForTransaction()
        {
            try
            {
                if (pgConnectionForTransaction != null)
                {
                    if (pgTransaction != null && pgTransaction.Connection != null)
                    {
                        IsolationLevel IsoTrnx = IsolationLevel.Unspecified;
                        try { IsoTrnx = pgTransaction.IsolationLevel; }
                        catch { }
                        if (IsoTrnx == IsolationLevel.ReadCommitted)
                        {
                            pgTransaction.Rollback();
                            if (pgTransaction.Connection != null && pgTransaction.Connection.State != ConnectionState.Closed)
                            {
                                pgTransaction.Connection.Close();
                            }
                        }

                        pgTransaction.Dispose();
                        pgTransaction = null;
                    }

                    if (pgConnectionForTransaction.State != ConnectionState.Closed)
                    {
                        pgConnectionForTransaction.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Libera recursos de un DataReader.
        /// </summary>
        /// <param name="pDataReader">Objeto DataReader a liberar de memoria.</param>
        protected void DisposeReader(ref IDataReader pDataReader)
        {
            try
            {
                if (pDataReader != null)
                {
                    if (!pDataReader.IsClosed)
                        pDataReader.Close();
                    pDataReader.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Libera recursos del comando.
        /// </summary>
        protected void DisposeCommand()
        {
            try
            {
                if (pgCommand != null)
                    pgCommand.Dispose();
                pgCommand = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Devuelve un DataReader con el resultado de la consulta.
        /// </summary>
        /// <param name="pQuery">Consulta SQL.</param>
        /// <returns></returns>
        protected IDataReader GetList(string pQuery)
        {
            IDataReader vDataReader = null;
            using (NpgsqlCommand vCommand = new NpgsqlCommand(pQuery, pgConnection))
            {
                OpenConnection();
                try
                {
                    vDataReader = vCommand.ExecuteReader();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    //this.DisposeConnection();
                }
            }
            return vDataReader;
        }

        /// <summary>
        /// Devuelve un DataReader con el resultado del comando.
        /// </summary>
        /// <param name="pCommand">Comando de Consulta.</param>
        /// <returns></returns>
        protected IDataReader GetList(ref NpgsqlCommand pCommand)
        {
            IDataReader vDataReader = null;
            if (pCommand.Connection == null)// || vCommand.Connection.State != ConnectionState.Open)
            {
                pCommand.Connection = pgConnection;
            }
            OpenConnection();
            try
            {
                vDataReader = pCommand.ExecuteReader();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return vDataReader;
        }

        /// <summary>
        /// Devuelve un DataReader con el resultado de la llamada a un procedimiento almacenado.
        /// </summary>
        /// <param name="pStoreProcedure">Nombre del procedimiento almacenado.</param>
        /// <param name="pParameters">Parametros del procedimiento almacenado.</param>
        /// <returns></returns>
        protected IDataReader GetList(string pStoreProcedure, NpgsqlParameter[] pParameters)
        {
            IDataReader vDataReader = null;
            using (NpgsqlCommand vCommand = new NpgsqlCommand(pStoreProcedure, pgConnection))
            {
                NpgsqlParameter vParameter;
                try
                {
                    OpenConnection();
                    vCommand.CommandType = CommandType.StoredProcedure;
                    if (pParameters != null)
                    {
                        for (int vIndex = 0; vIndex < pParameters.GetLength(0); vIndex++)
                        {
                            if (pParameters[vIndex].Direction == ParameterDirection.Input)
                            {
                                vParameter = new NpgsqlParameter();
                                vParameter.ParameterName = pParameters[vIndex].ParameterName;
                                vParameter.DbType = pParameters[vIndex].DbType;
                                vParameter.Value = pParameters[vIndex].Value;
                                vParameter.Direction = ParameterDirection.Input;
                                vCommand.Parameters.Add(vParameter);
                            }
                            else
                            {
                                vParameter = new NpgsqlParameter();
                                vParameter.ParameterName = pParameters[vIndex].ParameterName;
                                vParameter.DbType = pParameters[vIndex].DbType;
                                vParameter.Value = pParameters[vIndex].Value;
                                vParameter.Direction = ParameterDirection.Output;
                                vCommand.Parameters.Add(vParameter);
                            }
                        }
                    }
                    vDataReader = vCommand.ExecuteReader();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return vDataReader;
        }

        /// <summary>
        /// Método que devuelve una determinada entidad.
        /// </summary>
        /// <typeparam name="T">Tipo Genérico.</typeparam>
        /// <param name="pQuery">Consulta SQL.</param>
        /// <param name="pReference">Indican si se cargaran las entidades enlazadas a la entidad.</param>
        /// <returns></returns>
        protected T FindSQL<T>(string pQuery) where T : Domain.Seedwork.Entity, new()
        {
            IDataReader pvDataReader = null;
            T vEntity = new T();
            try
            {
                pvDataReader = GetList(pQuery);

                if (pvDataReader.Read())
                {
                    vEntity = this.CargarH<T>(ref pvDataReader);
                }
                else
                {
                    vEntity = null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                DisposeReader(ref pvDataReader);
                DisposeCommand();
                DisposeConnection();
            }
            return vEntity;
        }

        /// <summary>
        /// Método que devuelve una determinada entidad.
        /// </summary>
        /// <typeparam name="T">Tipo Genérico.</typeparam>
        /// <param name="pCommand">Comando que tiene la consulta.</param>
        /// <param name="pReference">Indica si se cargaran entidades referenciadas a la entidad.</param>
        /// <returns></returns>
        protected T FindSQL<T>(ref NpgsqlCommand pCommand, Dictionary<string, object> parameters = null) where T : Domain.Seedwork.Entity, new()
        {
            IDataReader vDataReader = null;
            T vEntity = new T();
            if (parameters != null)
            {
                // pCommand.BindByName = true;
                pCommand.Parameters.Clear();
                BindParameter(pCommand, parameters);
            }
            try
            {
                vDataReader = GetList(ref pCommand);
                if (vDataReader.Read())
                {
                    vEntity = this.CargarH<T>(ref vDataReader);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                DisposeReader(ref vDataReader);
                pCommand.Dispose();
                DisposeCommand();
                DisposeConnection();
            }
            return vEntity;
        }

        /// <summary>
        /// Método que devuelve un listado de entidades.
        /// </summary>
        /// <typeparam name="T">Tipo Genérico.</typeparam>
        /// <param name="pCommand">Comando de consulta.</param>
        /// <param name="pReference">Indica si se cargaran entidades referenciadas a la entidad.</param>
        /// <returns></returns>
        protected List<T> ListSQL<T>(NpgsqlCommand pCommand, Dictionary<string, object> parameters = null) where T : Domain.Seedwork.Entity, new()
        {
            List<T> vList = new List<T>();
            IDataReader vDataReader = null;

            try
            {
                if (parameters != null)
                {
                    BindParameter(pCommand, parameters);
                }

                vDataReader = GetList(ref pCommand);
                while (vDataReader.Read())
                {
                    vList.Add(this.CargarH<T>(ref vDataReader));
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                DisposeReader(ref vDataReader);
                pCommand.Dispose();
                DisposeCommand();
                DisposeConnection();
            }
            return vList;
        }

        /// <summary>
        /// Devuelve un listado de entidades en base a un procedimiento almacenado.
        /// </summary>
        /// <typeparam name="T">Tipo Generico</typeparam>
        /// <param name="pStoreProcedure">Nombre del procedimiento almacenado.</param>
        /// <param name="pParameters">Parametros que se mandaran al procedimiento almacenado.</param>
        /// <param name="pReference">Indica si se cargaran entidades referenciadas a la entidad.</param>
        /// <returns></returns>
        protected List<T> ListSQLBySP<T>(string pStoreProcedure, NpgsqlParameter[] pParameters, params bool[] pReference) where T : Domain.Seedwork.Entity, new()
        {
            List<T> vList = new List<T>();
            IDataReader vDataReader = null;

            try
            {
                vDataReader = GetList(pStoreProcedure, pParameters);
                while (vDataReader.Read())
                {
                    vList.Add(this.CargarH<T>(ref vDataReader));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                DisposeReader(ref vDataReader);
                DisposeCommand();
                DisposeConnection();
            }
            return vList;
        }

        /// <summary>
        /// Devuelve un valor en base a una consulta SQL.
        /// </summary>
        /// <param name="pQuery">Consulta SQL a realizar.</param>
        /// <returns></returns>
        protected object GetValue(string pQuery)
        {
            object vValue = null;
            pgCommand = new NpgsqlCommand(pQuery, pgConnection);

            try
            {
                vValue = pgCommand.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                DisposeCommand();
                DisposeConnection();
            }
            return vValue;
        }

        /// <summary>
        /// Ejecuta el comando que se tiene como parametro.
        /// </summary>
        /// <param name="pCommand">Comando que tienen la instrucción.</param>
        /// <returns></returns>
        protected bool ExecuteNonQuery(NpgsqlCommand pCommand)
        {
            bool vResultado = false;
            try
            {
                OpenConnection();
                if (pCommand.Connection == null)
                {
                    pCommand.Connection = pgConnection;
                }
                pCommand.ExecuteNonQuery();
                vResultado = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return vResultado;
        }

        protected bool ExecuteNonQueryForTransaction(NpgsqlCommand pCommand)
        {
            bool vResultado = false;
            try
            {
                pCommand.ExecuteNonQuery();
                vResultado = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return vResultado;
        }

        protected bool ExecuteNonQueryForTransaction(NpgsqlCommand pCommand,  ref Int64 idReg)
        {
            bool vResultado = false;
            try
            {
                NpgsqlDataReader reader = pCommand.ExecuteReader();
                while (reader.Read())
                {
                    idReg = reader.GetInt32(0);
                }
                reader.Close();
                vResultado = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return vResultado;
        }

        /// <summary>
        /// Método que debe ser actualizado.
        /// </summary>
        /// <typeparam name="T">Tipo Genérico.</typeparam>
        /// <param name="pDataReader">DataReader que tiene el resultado de la consulta.</param>
        /// <param name="pReference">Indica si se cargaran entidades referenciadas a la entidad.</param>
        /// <returns></returns>
        protected virtual T CargarH<T>(ref IDataReader pDataReader) where T : Domain.Seedwork.Entity, new()
        {
            T vEntity = new T();
            var vPropiedades = vEntity.GetType().GetProperties();
            try
            {
                PropertyInfo[] propiedades = new PropertyInfo[vEntity.GetType().GetProperties().Count()];
                vEntity.GetType().GetProperties().CopyTo(propiedades, 0);
                propiedades = propiedades.Where(a => !typeof(Entity).GetProperties().Any(b => b.Name == a.Name)).ToArray();

                DataTable dtSchema = pDataReader.GetSchemaTable();

                foreach (PropertyInfo item in propiedades)
                {
                    if (!dtSchema.AsEnumerable().Any(a => a.Field<string>("ColumnName").ToUpper().Equals(item.Name)))
                    {
                        continue;
                    }
                    if (pDataReader[item.Name] == null ||
                        pDataReader[item.Name] == DBNull.Value)
                    {
                        vEntity.GetType().GetProperty(item.Name).SetValue(vEntity, null, null);
                    }
                    else if (vEntity.GetType().GetProperty(item.Name).PropertyType.FullName.Contains("Int64"))
                    {
                        vEntity.GetType().GetProperty(item.Name).SetValue(vEntity, Convert.ToInt64(pDataReader[item.Name].ToString()), null);
                    }
                    else if (vEntity.GetType().GetProperty(item.Name).PropertyType.FullName.Contains("Int32"))
                    {
                        vEntity.GetType().GetProperty(item.Name).SetValue(vEntity, Convert.ToInt32(pDataReader[item.Name].ToString()), null);
                    }
                    else if (vEntity.GetType().GetProperty(item.Name).PropertyType.FullName.Contains("Decimal"))
                    {
                        vEntity.GetType().GetProperty(item.Name).SetValue(vEntity, Convert.ToDecimal(pDataReader[item.Name]), null);
                    }
                    else if (vEntity.GetType().GetProperty(item.Name).PropertyType.FullName.Contains("Single"))
                    {
                        vEntity.GetType().GetProperty(item.Name).SetValue(vEntity, Convert.ToSingle(pDataReader[item.Name]), null);
                    }
                    else if (vEntity.GetType().GetProperty(item.Name).PropertyType.FullName.Contains("StringBuilder"))
                    {
                        vEntity.GetType().GetProperty(item.Name).SetValue(vEntity, new StringBuilder(pDataReader[item.Name].ToString()), null);
                    }
                    else if (vEntity.GetType().GetProperty(item.Name).PropertyType.FullName.Contains("String"))
                    {
                        vEntity.GetType().GetProperty(item.Name).SetValue(vEntity, pDataReader[item.Name].ToString(), null);
                    }
                    else if (vEntity.GetType().GetProperty(item.Name).PropertyType.FullName.Contains("Boolean"))
                    {
                        vEntity.GetType().GetProperty(item.Name).SetValue(vEntity, Convert.ToBoolean(int.Parse(pDataReader[item.Name].ToString())), null);
                    }
                    else if (vEntity.GetType().GetProperty(item.Name).PropertyType.FullName.Contains("DateTime"))
                    {
                        vEntity.GetType().GetProperty(item.Name).SetValue(vEntity, Convert.ToDateTime(pDataReader[item.Name]), null);
                    }
                    else if (vEntity.GetType().GetProperty(item.Name).PropertyType.FullName.Contains("Byte[]"))
                    {
                        vEntity.GetType().GetProperty(item.Name).SetValue(vEntity, (byte[])pDataReader[item.Name], null);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return (T)(Domain.Seedwork.Entity)vEntity;
        }

        private NpgsqlDbType getType(PropertyInfo Info)
        {
            NpgsqlDbType resul = NpgsqlDbType.Varchar;

            if (Info.PropertyType.FullName.Contains("Int32"))
            {
                resul = NpgsqlDbType.Integer;
            }
            else if (Info.PropertyType.FullName.Contains("Decimal"))
            {
                resul = NpgsqlDbType.Numeric;
            }
            else if (Info.PropertyType.FullName.Contains("Single"))
            {
                resul = NpgsqlDbType.Real;

            }
            else if (Info.PropertyType.FullName.Contains("String"))
            {
                resul = NpgsqlDbType.Varchar;
            }
            else if (Info.PropertyType.FullName.Contains("Boolean"))
            {
                resul = NpgsqlDbType.Char;
            }
            else if (Info.PropertyType.FullName.Contains("DateTime"))
            {
                resul = NpgsqlDbType.Date;
            }
            else if (Info.PropertyType.FullName.Contains("Byte[]"))
            {
                resul = NpgsqlDbType.Bytea;
            }

            return resul;
        }

        private NpgsqlDbType getType(Object pObjIn)
        {
            NpgsqlDbType resul = NpgsqlDbType.Varchar;
            System.Type type = typeof(String);
            if (pObjIn != null)
            {
                type = pObjIn.GetType();
            }

            if (type.FullName.Contains("Int32"))
            {
                resul = NpgsqlDbType.Integer;
            }
            else if (type.FullName.Contains("Decimal"))
            {
                resul = NpgsqlDbType.Numeric;
            }
            else if (type.FullName.Contains("Single"))
            {
                resul = NpgsqlDbType.Real;

            }
            else if (type.FullName.Contains("String"))
            {
                resul = NpgsqlDbType.Varchar;
            }
            else if (type.FullName.Contains("Boolean"))
            {
                resul = NpgsqlDbType.Char;
            }
            else if (type.FullName.Contains("DateTime"))
            {
                resul = NpgsqlDbType.Date;
            }
            else if (type.FullName.Contains("Byte[]"))
            {
                resul = NpgsqlDbType.Bytea;
            }

            return resul;
        }

        private void BindParameter(TEntity ObjIn, PropertyInfo[] propiedades)
        {
            foreach (var propiedad in propiedades)
            {
                var pgParameter = new NpgsqlParameter();
                pgParameter.ParameterName = string.Format(":{0}", propiedad.Name);
                pgParameter.Direction = ParameterDirection.Input;

                if (ObjIn.GetType().GetProperty(propiedad.Name).GetValue(ObjIn, null) == null)
                {
                    pgParameter.NpgsqlDbType = getType(propiedad);
                    pgParameter.NpgsqlValue = DBNull.Value;
                }
                else if (ObjIn.GetType().GetProperty(propiedad.Name).PropertyType.FullName.Contains("Int32"))
                {
                    pgParameter.NpgsqlDbType = NpgsqlDbType.Integer;
                    pgParameter.NpgsqlValue = Convert.ToInt32(ObjIn.GetType().GetProperty(propiedad.Name).GetValue(ObjIn, null));
                }
                else if (ObjIn.GetType().GetProperty(propiedad.Name).PropertyType.FullName.Contains("Decimal"))
                {
                    pgParameter.NpgsqlDbType = NpgsqlDbType.Numeric;
                    pgParameter.NpgsqlValue = Convert.ToDecimal(ObjIn.GetType().GetProperty(propiedad.Name).GetValue(ObjIn, null));
                }
                else if (ObjIn.GetType().GetProperty(propiedad.Name).PropertyType.FullName.Contains("Single"))
                {
                    pgParameter.NpgsqlDbType = NpgsqlDbType.Varchar;
                    pgParameter.NpgsqlValue = ObjIn.GetType().GetProperty(propiedad.Name).GetValue(ObjIn, null);
                }
                else if (ObjIn.GetType().GetProperty(propiedad.Name).PropertyType.FullName.Contains("String"))
                {
                    pgParameter.NpgsqlDbType = NpgsqlDbType.Varchar;
                    pgParameter.NpgsqlValue = ObjIn.GetType().GetProperty(propiedad.Name).GetValue(ObjIn, null);
                }
                else if (ObjIn.GetType().GetProperty(propiedad.Name).PropertyType.FullName.Contains("Boolean"))
                {
                    pgParameter.NpgsqlDbType = NpgsqlDbType.Char;
                    pgParameter.NpgsqlValue = Convert.ToInt32(Convert.ToBoolean(ObjIn.GetType().GetProperty(propiedad.Name).GetValue(ObjIn, null))).ToString();
                }
                else if (ObjIn.GetType().GetProperty(propiedad.Name).PropertyType.FullName.Contains("DateTime"))
                {
                    pgParameter.NpgsqlDbType = NpgsqlDbType.Date;
                    pgParameter.NpgsqlValue = Convert.ToDateTime(ObjIn.GetType().GetProperty(propiedad.Name).GetValue(ObjIn, null));
                }
                else if (ObjIn.GetType().GetProperty(propiedad.Name).PropertyType.FullName.Contains("Byte[]"))
                {
                    pgParameter.NpgsqlDbType = NpgsqlDbType.Bytea;
                    pgParameter.NpgsqlValue = ObjIn.GetType().GetProperty(propiedad.Name).GetValue(ObjIn, null);
                }

                if (pgParameter.NpgsqlDbType != null)
                {
                    pgCommand.Parameters.Add(pgParameter);
                }
            }
        }

        private void BindParameter(TEntity ObjIn, PropertyInfo[] propiedades, NpgsqlCommand comand, List<PropertyInfo> PrimaryKeys = null)
        {
            int vCont = 0;
            foreach (var propiedad in propiedades)
            {
                var vKeyName = PrimaryKeys == null ? null : PrimaryKeys.Where(x => x.Name == propiedad.Name).FirstOrDefault();
                var pgParameter = new NpgsqlParameter();
                pgParameter.ParameterName = string.Format(":{0}", propiedad.Name);
                pgParameter.Direction = ParameterDirection.Input;

                if (vKeyName == null || vKeyName.Name != propiedad.Name)
                {
                    if (ObjIn.GetType().GetProperty(propiedad.Name).GetValue(ObjIn, null) == null)
                    {
                        pgParameter.NpgsqlDbType = getType(propiedad);
                        pgParameter.NpgsqlValue = DBNull.Value;
                    }
                    else if (ObjIn.GetType().GetProperty(propiedad.Name).PropertyType.FullName.Contains("Int64")) {
                        pgParameter.NpgsqlDbType = NpgsqlDbType.Bigint;
                        pgParameter.NpgsqlValue = Convert.ToInt64(ObjIn.GetType().GetProperty(propiedad.Name).GetValue(ObjIn, null));
                    }
                    else if (ObjIn.GetType().GetProperty(propiedad.Name).PropertyType.FullName.Contains("Int32"))
                    {
                        pgParameter.NpgsqlDbType = NpgsqlDbType.Integer;
                        pgParameter.NpgsqlValue = Convert.ToInt32(ObjIn.GetType().GetProperty(propiedad.Name).GetValue(ObjIn, null));
                    }
                    else if (ObjIn.GetType().GetProperty(propiedad.Name).PropertyType.FullName.Contains("Decimal"))
                    {
                        pgParameter.NpgsqlDbType = NpgsqlDbType.Numeric;
                        pgParameter.NpgsqlValue = Convert.ToDecimal(ObjIn.GetType().GetProperty(propiedad.Name).GetValue(ObjIn, null));
                    }
                    else if (ObjIn.GetType().GetProperty(propiedad.Name).PropertyType.FullName.Contains("Single"))
                    {
                        pgParameter.NpgsqlDbType = NpgsqlDbType.Real;
                        pgParameter.NpgsqlValue = ObjIn.GetType().GetProperty(propiedad.Name).GetValue(ObjIn, null);
                    }
                    else if (ObjIn.GetType().GetProperty(propiedad.Name).PropertyType.FullName.Contains("StringBuilder"))
                    {
                        pgParameter.NpgsqlDbType = NpgsqlDbType.Varchar;
                        pgParameter.NpgsqlValue = ObjIn.GetType().GetProperty(propiedad.Name).GetValue(ObjIn, null).ToString();
                    }
                    else if (ObjIn.GetType().GetProperty(propiedad.Name).PropertyType.FullName.Contains("String"))
                    {
                        pgParameter.NpgsqlDbType = NpgsqlDbType.Varchar;
                        pgParameter.NpgsqlValue = ObjIn.GetType().GetProperty(propiedad.Name).GetValue(ObjIn, null);
                    }
                    else if (ObjIn.GetType().GetProperty(propiedad.Name).PropertyType.FullName.Contains("Boolean"))
                    {
                        pgParameter.NpgsqlValue = NpgsqlDbType.Char;
                        pgParameter.NpgsqlValue = Convert.ToInt32(Convert.ToBoolean(ObjIn.GetType().GetProperty(propiedad.Name).GetValue(ObjIn, null))).ToString();
                    }
                    else if (ObjIn.GetType().GetProperty(propiedad.Name).PropertyType.FullName.Contains("DateTime"))
                    {
                        pgParameter.NpgsqlValue = NpgsqlDbType.Date;
                        pgParameter.NpgsqlValue = Convert.ToDateTime(ObjIn.GetType().GetProperty(propiedad.Name).GetValue(ObjIn, null));
                    }
                    else if (ObjIn.GetType().GetProperty(propiedad.Name).PropertyType.FullName.Contains("Byte[]"))
                    {
                        pgParameter.NpgsqlDbType = NpgsqlDbType.Bytea;
                        pgParameter.NpgsqlValue = ObjIn.GetType().GetProperty(propiedad.Name).GetValue(ObjIn, null);
                    }

                    if (pgParameter.NpgsqlDbType != null)
                    {
                        comand.Parameters.Add(pgParameter);
                    }
                }

                vCont++;
            }
        }

        private void BindParameter(NpgsqlCommand pCommand, Dictionary<string, object> parameters)
        {
            foreach (var propiedad in parameters)
            {
                var pgParameter = new NpgsqlParameter();
                pgParameter.Direction = ParameterDirection.Input;
                pgParameter.ParameterName = string.Format("{0}", propiedad.Key);

                if (propiedad.Value == null)
                {
                    pgParameter.NpgsqlValue = DBNull.Value;
                }

                else if (propiedad.Value.GetType().FullName.Contains("Int32"))
                {
                    pgParameter.NpgsqlDbType = NpgsqlDbType.Integer;
                    pgParameter.NpgsqlValue = Convert.ToInt32(propiedad.Value);
                }
                else if (propiedad.Value.GetType().FullName.Contains("Decimal"))
                {
                    pgParameter.NpgsqlDbType = NpgsqlDbType.Numeric;
                    pgParameter.NpgsqlValue = Convert.ToDecimal(propiedad.Value);
                }
                else if (propiedad.Value.GetType().FullName.Contains("Single"))
                {
                    pgParameter.NpgsqlDbType = NpgsqlDbType.Real;
                    pgParameter.NpgsqlValue = propiedad.Value;
                }
                else if (propiedad.Value.GetType().FullName.Contains("String"))
                {
                    pgParameter.NpgsqlDbType = NpgsqlDbType.Varchar;
                    pgParameter.NpgsqlValue = propiedad.Value;
                }
                else if (propiedad.Value.GetType().FullName.Contains("Boolean"))
                {
                    pgParameter.NpgsqlDbType = NpgsqlDbType.Char;
                    pgParameter.NpgsqlValue = Convert.ToInt32(propiedad.Value).ToString();
                }
                else if (propiedad.Value.GetType().FullName.Contains("DateTime"))
                {
                    pgParameter.NpgsqlDbType = NpgsqlDbType.Date;
                    pgParameter.NpgsqlValue = Convert.ToDateTime(propiedad.Value);
                }
                else if (propiedad.Value.GetType().FullName.Contains("StringBuilder"))
                {
                    pgParameter.NpgsqlDbType = NpgsqlDbType.Varchar;
                    pgParameter.NpgsqlValue = propiedad.Value;

                }
                else if (propiedad.Value.GetType().FullName.Contains("Byte[]"))
                {
                    pgParameter.NpgsqlDbType = NpgsqlDbType.Bytea;
                    pgParameter.NpgsqlValue = propiedad.Value;
                }

                if (pgParameter.NpgsqlDbType != null)
                {
                    pCommand.Parameters.Add(pgParameter);
                }
            }
        }

        private object GetIdForEntity(string nameEntidad, string IdInstitucion, string IdUnidadEjecutora, ParamOut Id)
        {
            StringBuilder vSQL = new StringBuilder();
            object vValue = null;
            OpenConnectionForTransaction();
            vSQL.Append("select nvl(max(" + IdInstitucion + "),0) from");
            vSQL.Append(" " + nameEntidad);
            pgCommand = new NpgsqlCommand(vSQL.ToString(), pgConnectionForTransaction);
            vValue = pgCommand.ExecuteScalar();
            vValue = vValue == null ? 1 : Convert.ToDecimal(vValue) + 1;
            return vValue;
        }

        #endregion

        #region Metodos

        /// <summary>
        /// Realiza Commit de todos los cambios que se compartan en la conexion a la base de datos.
        /// </summary>
        public void CommitChanges()
        {
            try
            {
                if (pgTransaction != null)
                {
                    pgTransaction.Commit();
                    pgTransaction.Dispose();
                    pgTransaction = null;
                }
                if (HandlerCommite != null)
                {
                    HandlerCommite();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                this.DisposeConnectionForTransaction();
            }
        }

        /// <summary>
        /// Realiza Rollback de todos los cambios que se hayan realizado a la base de datos.
        /// </summary>
        public void RollbackChanges()
        {
            try
            {
                if (pgTransaction != null)
                {
                    pgTransaction.Rollback();
                    pgTransaction.Dispose();
                    pgTransaction = null;
                }
                this.DisposeConnectionForTransaction();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Devuelve una consulta en un DataSet no tipado.
        /// </summary>
        /// <param name="pQuery">Consulta.</param>
        /// <returns></returns>
        public DataSet GetDataSet(string pQuery)
        {
            DataSet vDataSet = new DataSet();
            using (NpgsqlDataAdapter vDataAdapter = new NpgsqlDataAdapter(pQuery, pgConnection))
            {
                OpenConnection();
                try
                {
                    vDataAdapter.Fill(vDataSet);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    DisposeConnection();
                }
            }
            return vDataSet;
        }

        /// <summary>
        /// Método que devuelve un listado de entidades.
        /// </summary>
        /// <typeparam name="T">Tipo Genérico.</typeparam>
        /// <param name="pQuery">Consulta SQL a realizar.</param>
        /// <param name="pReference">Indica si se cargaran entidades referenciadas a la entidad.</param>
        /// <returns></returns>
        public List<T> ListSQL<T>(string pQuery, Dictionary<string, object> parameters = null) where T : Domain.Seedwork.Entity, new()
        {
            List<T> vList = new List<T>();
            IDataReader vDataReader = null;

            try
            {
                vDataReader = GetList(pQuery);
                while (vDataReader.Read())
                {
                    vList.Add(this.CargarH<T>(ref vDataReader));
                }

            }
            catch (Exception ex)
            {
                ////throw ex;
            }
            finally
            {
                DisposeReader(ref vDataReader);
                DisposeCommand();
                DisposeConnection();
            }
            return vList;
        }

        /// <summary>
        /// Guarda los datos de la entidad.
        /// </summary>
        /// <param name="ObjIn"></param>
        /// <returns></returns>
        public virtual TEntity SaveChanges(TEntity ObjIn)
        {
            string vResultado = "";
            TEntity vEntity = ObjIn;//new TEntity();
            int vContador = 0;
            int vCantidadPropiedades = 0;
            string SeqObtain = string.Empty;

            var vPropiedades = vEntity.GetType().GetProperties();
            int vEstado = (int)ObjIn.State;
            StringBuilder vSQL = new StringBuilder();

            //Reflection cantidad de propiedades
            int vCantidadPropiedadesBase = typeof(Entity).GetProperties().Count();
            int vCantidadPropiedadesIgnore = vPropiedades.Where(a => a.GetCustomAttributes(typeof(Ignore), false).Count() > 0).Count();
            vCantidadPropiedades = vPropiedades.Where(a => a.CanRead).Count() - vCantidadPropiedadesBase - vCantidadPropiedadesIgnore; //Descuento de campo para omitir propiedades base.

            OpenConnectionForTransaction();
            if (pgTransaction == null) pgTransaction = pgConnectionForTransaction.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);
            if (pgCommandForTransaction == null)
            {
                pgCommandForTransaction = new NpgsqlCommand();
                pgCommandForTransaction.CommandType = CommandType.Text;
                pgCommandForTransaction.Connection = pgConnectionForTransaction;
                pgCommandForTransaction.Transaction = pgTransaction;
            }

            #region Identificamos si alguno de los contratos tiene llaves
            PropertyInfo[] propiedades = new PropertyInfo[vEntity.GetType().GetProperties().Count()];
            vEntity.GetType().GetProperties().CopyTo(propiedades, 0);

            PropertyInfo[] PrimaryKeys = propiedades.Where(a =>
                a.GetCustomAttributes(typeof(Key), false).Count() > 0
                ).ToArray();
            #endregion

            #region Ignoramos las propiedades configuradas con el atributo Ignore

            propiedades = propiedades.Where(a => !typeof(Entity).GetProperties().Any(b => b.Name == a.Name)
                                //&& a.GetCustomAttributes(typeof(Ignore), false).Count() == 0
                                && !a.GetCustomAttributes(typeof(Ignore), false).Any(x => x != null
                                                                                     && (x as Ignore).Update == true
                                                                                     && (x as Ignore).Insert == true)
                                ).ToArray();

            if (ObjIn.State == Domain.Seedwork.Enumerators.State.Insert)
            {
                propiedades = propiedades.Where(a => !typeof(Entity).GetProperties().Any(b => b.Name == a.Name)
                //&& a.GetCustomAttributes(typeof(Ignore), false).Count() == 0
                && !a.GetCustomAttributes(typeof(Ignore), false).Any(x => x != null && (x as Ignore).Insert == true)
                ).ToArray();

            }
            else if (ObjIn.State == Domain.Seedwork.Enumerators.State.Update)
            {
                propiedades = propiedades.Where(a => !typeof(Entity).GetProperties().Any(b => b.Name == a.Name)
                                //&& a.GetCustomAttributes(typeof(Ignore), false).Count() == 0
                                && !a.GetCustomAttributes(typeof(Ignore), false).Any(x => x != null && (x as Ignore).Update == true)
                                ).ToArray();

            }
            else if (ObjIn.State == Domain.Seedwork.Enumerators.State.Delete)
            {
                propiedades = propiedades.Where(a => !typeof(Entity).GetProperties().Any(b => b.Name == a.Name)
                                //&& a.GetCustomAttributes(typeof(Ignore), false).Count() == 0
                                && !a.GetCustomAttributes(typeof(Ignore), false).Any(x => x != null && (x as Ignore).Delete == true)
                                ).ToArray();

            }

            #endregion

            var lPrimaryKeys = PrimaryKeys.ToList();
            var lPropiedades = propiedades.ToList();

            List<NpgsqlParameter> lPropertySequence = new List<NpgsqlParameter>();//variable para administrar las propiedades que seran secuencias

            #region Construccioin de cadena de comando
            switch (ObjIn.State)
            {
                case Domain.Seedwork.Enumerators.State.None:
                    throw new Exception(Resources.Messages.information_EntitySaveChangesIncorrectState);
                    break;
                case Domain.Seedwork.Enumerators.State.Insert:

                    foreach (var PrimaryKey in PrimaryKeys)
                    {
                        string vIdInstitucion = ParametrosGenericos.IdGenerico;
                        string vIdUnidadEjecutora = ParametrosGenericos.IdUnidadGenerica;

                        if (!string.IsNullOrEmpty(ObjIn.Institucion) && !string.IsNullOrEmpty(ObjIn.UnidadEjecutora))
                        {
                            vIdInstitucion = ObjIn.Institucion;
                            vIdUnidadEjecutora = ObjIn.UnidadEjecutora;
                        }

                        ///Rescatamos el id de la trasaccion
                        NpgsqlParameter parametroKeySequence = new NpgsqlParameter();
                        parametroKeySequence.ParameterName = PrimaryKey.Name + "Sq";
                        switch (PrimaryKey.PropertyType.Name)
                        {
                            case "Int32":
                                parametroKeySequence.NpgsqlDbType = NpgsqlDbType.Integer;
                                break;
                            case "DateTime":
                                parametroKeySequence.NpgsqlDbType = NpgsqlDbType.Date;
                                break;
                            case "Int64":
                                parametroKeySequence.NpgsqlDbType = NpgsqlDbType.Integer;
                                break;
                            case "Decimal":
                                parametroKeySequence.NpgsqlDbType = NpgsqlDbType.Numeric;
                                break;
                            default:
                                parametroKeySequence.NpgsqlDbType = NpgsqlDbType.Varchar;
                                parametroKeySequence.Size = 200;
                                break;
                        }
                        parametroKeySequence.Direction = ParameterDirection.ReturnValue;
                        lPropertySequence.Add(parametroKeySequence);
                    }

                    vSQL.AppendLine(String.Format("INSERT INTO {0} (", vEntity.GetType().Name));
                    for (vContador = 0; vContador < propiedades.Length; vContador++)
                    {
                        if (vContador == 0)
                        {
                            vSQL.AppendLine(propiedades[vContador].Name);
                        }
                        else
                        {
                            vSQL.AppendLine(", " + propiedades[vContador].Name);
                        }
                    }
                    vSQL.AppendLine(") VALUES (");

                    for (vContador = 0; vContador < propiedades.Length; vContador++)
                    {
                        var lk = lPrimaryKeys.Where(x => x.Name == propiedades[vContador].Name).FirstOrDefault();

                        if (lk != null && lk.Name == propiedades[vContador].Name)
                        {
                            string vSequence = ((Domain.Seedwork.Key)(lk.GetCustomAttributes(typeof(Domain.Seedwork.Key), false).FirstOrDefault())).SequenceName;
                            if (string.IsNullOrEmpty(vSequence))
                            {
                                vSequence = "SQ_" + vEntity.GetType().Name;
                            }
                            vSQL.AppendLine(String.Format((vContador == 0 ? "" : ", ") + " {0}", " nextval('" + vSequence + "') "));
                        }
                        else
                        {
                            vSQL.AppendLine(String.Format((vContador == 0 ? "" : ", ") + " :{0}", propiedades[vContador].Name));
                        }
                    }
                    vSQL.AppendLine(")");

                    vContador = 0;
                    //                    if (lPropertySequence.Count > 0)
                    //                    {
                    vSQL.AppendLine(" returning ");
                    foreach (NpgsqlParameter propSequence in lPropertySequence)
                    {
                        vSQL.AppendLine(String.Format((vContador == 0 ? "" : ", ") + " {0}", propSequence.ParameterName.Replace("Sq", "")));
                    }

                    //vSQL.AppendLine(" into ");

                    //foreach (NpgsqlParameter propSequence in lPropertySequence)
                    //{
                    //    vSQL.AppendLine(String.Format((vContador == 0 ? "" : ", ") + " :{0}", propSequence.ParameterName));
                    //}
                    //                    }
                    break;
                case Domain.Seedwork.Enumerators.State.Update:

                    vSQL.AppendLine(String.Format("UPDATE {0} SET ", vEntity.GetType().Name));
                    lPropiedades = propiedades.Where(x => !PrimaryKeys.Any(h => h.Name == x.Name)).ToList();
                    foreach (var item in lPropiedades)
                    {
                        vSQL.AppendLine((lPropiedades.IndexOf(item) == 0 ? "" : ",") + String.Format("{0}= :{0}", item.Name));
                    }
                    vSQL.AppendLine(" WHERE ");

                    foreach (var PrimaryKey in lPrimaryKeys)
                    {
                        vSQL.AppendLine(lPrimaryKeys.IndexOf(PrimaryKey) > 0 ? " AND " : "" + String.Format("  {0}= :{0}", PrimaryKey.Name));
                    }

                    break;
                case Domain.Seedwork.Enumerators.State.Delete:
                    vSQL.AppendLine(String.Format("DELETE FROM {0} ", vEntity.GetType().Name));
                    vSQL.AppendLine(" WHERE ");

                    foreach (var PrimaryKey in lPrimaryKeys)
                    {
                        vSQL.AppendLine(lPrimaryKeys.IndexOf(PrimaryKey) > 0 ? " AND " : "" + String.Format("  {0}= :{0}", PrimaryKey.Name));
                    }
                    break;
                case Domain.Seedwork.Enumerators.State.Executed:
                    throw new Exception(Resources.Messages.information_EntitySaveChangesIncorrectState);
                    break;
                case Domain.Seedwork.Enumerators.State.Selected:
                    throw new Exception(Resources.Messages.information_EntitySaveChangesIncorrectState);
                    break;
                case Domain.Seedwork.Enumerators.State.Flag1:
                    throw new Exception(Resources.Messages.information_EntitySaveChangesIncorrectState);
                    break;
                case Domain.Seedwork.Enumerators.State.Flag2:
                    throw new Exception(Resources.Messages.information_EntitySaveChangesIncorrectState);
                    break;
                case Domain.Seedwork.Enumerators.State.Flag3:
                    throw new Exception(Resources.Messages.information_EntitySaveChangesIncorrectState);
                    break;
                default:
                    throw new Exception(Resources.Messages.information_EntitySaveChangesIncorrectState);
                    break;
            }

            #endregion

            pgCommandForTransaction.CommandText = vSQL.ToString();
            pgCommandForTransaction.Parameters.Clear();

            #region Bind Parametros
            switch (ObjIn.State)
            {
                case Domain.Seedwork.Enumerators.State.None:
                    throw new Exception(Resources.Messages.information_EntitySaveChangesIncorrectState);
                    break;
                case Domain.Seedwork.Enumerators.State.Insert:
                    BindParameter(ObjIn, propiedades, pgCommandForTransaction, PrimaryKeys.ToList());
                    pgCommandForTransaction.Parameters.AddRange(lPropertySequence.ToArray());
                    break;
                case Domain.Seedwork.Enumerators.State.Update:
                    BindParameter(ObjIn, propiedades, pgCommandForTransaction);
                    break;
                case Domain.Seedwork.Enumerators.State.Delete:
                    BindParameter(ObjIn, PrimaryKeys, pgCommandForTransaction);
                    //BindParameter(ObjIn, propiedades);
                    break;
                case Domain.Seedwork.Enumerators.State.Executed:
                    throw new Exception(Resources.Messages.information_EntitySaveChangesIncorrectState);
                    break;
                case Domain.Seedwork.Enumerators.State.Selected:
                    throw new Exception(Resources.Messages.information_EntitySaveChangesIncorrectState);
                    break;
                case Domain.Seedwork.Enumerators.State.Flag1:
                    throw new Exception(Resources.Messages.information_EntitySaveChangesIncorrectState);
                    break;
                case Domain.Seedwork.Enumerators.State.Flag2:
                    throw new Exception(Resources.Messages.information_EntitySaveChangesIncorrectState);
                    break;
                case Domain.Seedwork.Enumerators.State.Flag3:
                    throw new Exception(Resources.Messages.information_EntitySaveChangesIncorrectState);
                    break;
                default:
                    throw new Exception(Resources.Messages.information_EntitySaveChangesIncorrectState);
                    break;
            }

            #endregion

            //Ejecutamos la consulta.
            //dejamos la tranasccion pendiente hasta que se realice el commite
            long idReg = 0;
            ExecuteNonQueryForTransaction(pgCommandForTransaction, ref idReg);

            ////aqui seteamos las propiedades que fueron marcadas con Key(ISequence = true)
            foreach (NpgsqlParameter propertySequence in lPropertySequence)
            {
                string name = propertySequence.ParameterName.Replace("Sq", "");
                vEntity.GetType().GetProperty(name).SetValue(vEntity, Convert.ToDecimal(idReg), null);
            }

            //delegado para compartir memoria
            if (HandlerSaveChange != null)
            {
                HandlerSaveChange(ObjIn);
            }

            return ObjIn;
        }

        /// <summary>
        /// Ejecuta una transaccion a un listado de entidades
        /// </summary>
        /// <param name="pListado">Listado de entidades</param>
        public virtual void SaveChanges(IList pListado)
        {
            foreach (Entity item in pListado)
            {
                SaveChanges(item as TEntity);
            }
        }

        /// <summary>
        /// Devuelve un registro por entidad.
        /// </summary>
        /// <param name="pId"></param>
        /// <param name="ObjIn"></param>
        /// <returns></returns>
        public virtual T GetByID<T>(params object[] args) where T : Domain.Seedwork.Entity, new()
        {
            StringBuilder vSQL = new StringBuilder();
            T vEntidad = new T();

            ///Identificamos si alguno de los contratos tiene llaves key
            PropertyInfo[] propiedades = new PropertyInfo[vEntidad.GetType().GetProperties().Count()];
            vEntidad.GetType().GetProperties().CopyTo(propiedades, 0);
            PropertyInfo[] PrimaryKeys = propiedades.Where(a =>
                           a.GetCustomAttributes(typeof(Key), false).Count() > 0
                           ).ToArray();

            var lPrimaryKeys = PrimaryKeys.ToList();

            ///Identificamos las relaciones hijas del objeto
            PropertyInfo[] ForeingKeys = propiedades.Where(a =>
                           a.GetCustomAttributes(typeof(Relation), false).Count() > 0
                           ).ToArray();

            ///recuperamos el objeto
            if (PrimaryKeys.Count() > 0)
            {
                vSQL.Append("select * from");
                vSQL.Append(" " + vEntidad.GetType().Name);
                vSQL.Append(" where ");
                foreach (var PrimaryKey in lPrimaryKeys)
                {
                    vSQL.AppendLine(lPrimaryKeys.IndexOf(PrimaryKey) > 0 ? " AND " : "" + String.Format("  {0}= '{1}'", PrimaryKey.Name, args[lPrimaryKeys.IndexOf(PrimaryKey)]));
                }

                vEntidad = this.FindSQL<T>(vSQL.ToString());
            }
            else
            {
                throw new Exception("Debe definir alguna propiedad con el atributo Key");
            }

            if (vEntidad == null)
            {
                return vEntidad;
            }
            ///recuperamos los hijos del objeto
            foreach (var item in ForeingKeys)
            {
                Relation attribute = Attribute.GetCustomAttributes(item).ToList<System.Attribute>().Where(a => a.GetType() == typeof(Relation)).FirstOrDefault() as Relation;
                Type typeChild = item.PropertyType.GetGenericArguments()[0];
                vSQL.Clear();

                if (string.IsNullOrEmpty(attribute.NameProcedure))
                {
                    string value = "x";
                    if (vEntidad.GetType().GetProperty(attribute.KeySource).GetValue(vEntidad, null) != null)
                    {
                        value = vEntidad.GetType().GetProperty(attribute.KeySource).GetValue(vEntidad, null).ToString();
                    }
                    vSQL.AppendLine("SELECT * FROM ");
                    vSQL.AppendLine(typeChild.Name);
                    vSQL.AppendLine(" WHERE " + attribute.KeyTarget + " = '" + value + "'");
                    MethodInfo method = this.GetType().GetMethod("ListSQL");
                    MethodInfo genericMethod = method.MakeGenericMethod(new Type[] { typeChild });
                    item.SetValue(vEntidad, genericMethod.Invoke(this, new Object[] { vSQL.ToString(), null }), null);

                }
                else
                {
                    MethodInfo method = this.GetType().GetMethod("GetListByProcedureBD");
                    MethodInfo genericMethod = method.MakeGenericMethod(new Type[] { typeChild });
                    item.SetValue(vEntidad, genericMethod.Invoke(this, new Object[] { attribute.NameProcedure, new Object[] { vEntidad.GetType().GetProperty(attribute.KeySource).GetValue(vEntidad, null) } }), null);
                }

            }

            return vEntidad;
        }

        /// <summary>
        /// Devuelve un registro por entidad.
        /// </summary>
        /// <param name="pId"></param>
        /// <param name="ObjIn"></param>
        /// <returns></returns>
        public virtual TEntity GetByID(int pId)
        {
            StringBuilder vSQL = new StringBuilder();
            TEntity vEntidad = new TEntity();

            vSQL.Append("select * from");
            vSQL.Append(" " + vEntidad.GetType().Name);
            vSQL.Append(" where ID" + vEntidad.GetType().Name);
            vSQL.Append(" = " + pId);
            vEntidad = this.FindSQL<TEntity>(vSQL.ToString());

            return vEntidad;
        }

        /// <summary>
        /// Devuelve el listado de la entidad.
        /// </summary>
        /// <returns></returns>
        public virtual List<TEntity> GetAll()
        {
            StringBuilder vSQL = new StringBuilder();
            TEntity vEntidad = new TEntity();
            List<TEntity> vListado = new List<TEntity>();

            vSQL.Append("select * from");
            vSQL.Append(" " + vEntidad.GetType().Name);
            vListado = ListSQL<TEntity>(vSQL.ToString(), null);

            return vListado;
        }

        /// <summary>
        /// Devuelve el listado de la entidad.
        /// </summary>
        /// <returns></returns>
        public virtual List<T> GetAll<T>() where T : Domain.Seedwork.Entity, new()
        {
            StringBuilder vSQL = new StringBuilder();
            T vEntidad = new T();
            List<T> vListado = new List<T>();

            vSQL.Append("select * from");
            vSQL.Append(" " + vEntidad.GetType().Name);
            vListado = ListSQL<T>(vSQL.ToString(), null);

            return vListado;
        }

        /// <summary>
        /// Devuelve el listado de la entidad mediante el filtro de las expresiones.
        /// </summary>
        /// <typeparam name="T">Tipo de POCO a devolver</typeparam>
        /// <param name="pExpression">Expression entity que corresponde a lambda</param>
        /// <param name="pTransacction">solo usar este parametro en true en caso de tener una transaccion ya inicializada</param>
        /// <returns></returns>
        public virtual List<T> Where<T>(Func<T, ExpressionEntity> pExpression, bool pTransacction = false) where T : Domain.Seedwork.Entity, new()
        {
            //OracleCommand vCommand = new OracleCommand();
            pgCommand = new NpgsqlCommand();
            if (pTransacction)
            {
                if (pgCommandForTransaction == null)
                {
                    pgCommandForTransaction = pgCommand;
                }
                else
                {
                    pgCommand = pgCommandForTransaction;
                }
            }

            T entity = new T();
            var vResulExpression = pExpression(entity);
            if (!vResulExpression.Valid)
            {
                throw new Exception("Expression no valid");
            }
            StringBuilder vSQL = new StringBuilder();
            List<T> vListado = new List<T>();
            ExpressionEntity.ReFormulation(vResulExpression);
            vSQL.AppendLine("SELECT * FROM " + typeof(T).Name);
            vSQL.AppendLine(" WHERE " + vResulExpression.Formulation);
            if (pTransacction)
            {
                OpenConnectionForTransaction();
                if (pgTransaction == null) pgTransaction = pgConnectionForTransaction.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);
                pgCommandForTransaction.Connection = pgConnectionForTransaction;
                pgCommandForTransaction.Transaction = pgTransaction;

            }
            pgCommand.Parameters.Clear();
            pgCommand.CommandType = CommandType.Text;
            pgCommand.CommandText = vSQL.ToString();

            vListado = ListSQL<T>(pgCommand, vResulExpression.Parameters);

            //DisposeCommand
            DisposeCommand();

            return vListado;
        }

        /// <summary>
        /// Devuelve el listado de la entidad mediante el filtro de las expresiones.
        /// </summary>
        /// <typeparam name="T">Tipo de POCO a devolver</typeparam>
        /// <param name="pExpression">Expression entity que corresponde a lambda</param>
        /// <param name="pTransacction">solo usar este parametro en true en caso de tener una transaccion ya inicializada</param>
        /// <returns></returns>
        public virtual List<T> WhereDistinct<T>(Func<T, ExpressionEntity> pExpression, bool pTransacction = false) where T : Domain.Seedwork.Entity, new()
        {
            //OracleCommand vCommand = new OracleCommand();
            pgCommand = new NpgsqlCommand();
            if (pTransacction)
            {
                if (pgCommandForTransaction == null)
                {
                    pgCommandForTransaction = pgCommand;
                }
                else
                {
                    pgCommand = pgCommandForTransaction;
                }
            }

            T entity = new T();
            var vResulExpression = pExpression(entity);
            if (!vResulExpression.Valid)
            {
                throw new Exception("Expression no valid");
            }

            StringBuilder vSQL = new StringBuilder();
            List<T> vListado = new List<T>();
            ExpressionEntity.ReFormulation(vResulExpression);
            vSQL.AppendLine("SELECT DISTINCT * FROM " + typeof(T).Name);
            vSQL.AppendLine(" WHERE " + vResulExpression.Formulation);
            if (pTransacction)
            {
                OpenConnectionForTransaction();
                if (pgTransaction == null) pgTransaction = pgConnectionForTransaction.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);
                pgCommandForTransaction.Connection = pgConnectionForTransaction;
                pgCommandForTransaction.Transaction = pgTransaction;
            }

            pgCommand.Parameters.Clear();
            pgCommand.CommandType = CommandType.Text;
            pgCommand.CommandText = vSQL.ToString();

            vListado = ListSQL<T>(pgCommand, vResulExpression.Parameters);

            //DisposeCommand
            DisposeCommand();

            return vListado;
        }

        /// <summary>
        /// Devuelve una lista de entidades a partir de un procedimiento almacenado
        /// </summary>
        /// <param name="nameProcedure">nombre completo del procedimiento</param>
        /// <param name="param"> parametros de entrada y salida, para los parametros de salia use el objeto ParamOut </param>
        /// <returns></returns>
        public virtual List<T> GetListByProcedureBD<T>(string nameProcedure, params Object[] param) where T : Domain.Seedwork.Entity, new()
        {
            List<T> vListado = new List<T>();

            pgCommand = new NpgsqlCommand();
            pgCommand.CommandText = nameProcedure;
            pgCommand.Connection = pgConnection;
            pgCommand.CommandText = "declare begin " + Schema + "." + nameProcedure + " (";
            string nomParametro = "parametro";
            int a = 0;
            Dictionary<int, NpgsqlParameter> parametroSalida = new Dictionary<int, NpgsqlParameter>();

            foreach (object item in param)
            {
                pgCommand.CommandText += ":" + nomParametro + a.ToString() + ",";

                NpgsqlParameter parametro = null;

                if (item == null)
                {
                    parametro = new NpgsqlParameter
                    {
                        ParameterName = ":" + nomParametro + a.ToString(),
                        Value = null
                    };
                    a++;
                    pgCommand.Parameters.Add(parametro);
                    continue;
                }

                parametro = new NpgsqlParameter
                {
                    ParameterName = ":" + nomParametro + a.ToString(),
                    Direction = item.GetType() == typeof(ParamOut) ? ParameterDirection.Output : ParameterDirection.Input
                };
                switch (item.GetType().Name)
                {
                    case "Int32":
                        parametro.NpgsqlDbType = NpgsqlDbType.Integer;
                        parametro.Value = item.GetType() == typeof(ParamOut) ? (item as ParamOut).Valor : item;
                        break;
                    case "Int64":
                        parametro.NpgsqlDbType = NpgsqlDbType.Integer;
                        parametro.Value = item.GetType() == typeof(ParamOut) ? (item as ParamOut).Valor : item;
                        break;
                    case "Decimal":
                        parametro.NpgsqlDbType = NpgsqlDbType.Numeric;
                        parametro.Value = item.GetType() == typeof(ParamOut) ? (item as ParamOut).Valor : item;
                        break;
                    case "String":
                        parametro.NpgsqlDbType = NpgsqlDbType.Varchar;
                        parametro.Value = item.GetType() == typeof(ParamOut) ? (item as ParamOut).Valor : item;
                        break;
                    case "Boolean":
                        parametro.NpgsqlDbType = NpgsqlDbType.Char;
                        parametro.Value = Convert.ToInt32((item.GetType() == typeof(ParamOut) ? (item as ParamOut).Valor : item));
                        break;
                    case "DateTime":
                        parametro.NpgsqlDbType = NpgsqlDbType.Date;
                        parametro.Value = item.GetType() == typeof(ParamOut) ? (item as ParamOut).Valor : item;
                        break;
                    case "Byte[]":
                        parametro.NpgsqlDbType = NpgsqlDbType.Bytea;
                        parametro.Value = item.GetType() == typeof(ParamOut) ? (item as ParamOut).Valor : item;
                        break;
                    default:
                        break;
                }
                if (item.GetType() == typeof(ParamOut))
                {
                    parametro.DbType = DbType.String;
                    parametro.Value = DBNull.Value;
                    parametro.Direction = ParameterDirection.Output;
                    parametro.Size = (item as ParamOut).Size;
                    parametroSalida.Add(a, parametro);
                }
                a++;
                pgCommand.Parameters.Add(parametro);

            }

            pgCommand.CommandText += ":cursor_resul); end;";
            var pgParameter = new NpgsqlParameter();
            pgParameter.ParameterName = ":cursor_resul";
            pgParameter.NpgsqlDbType = NpgsqlDbType.Refcursor;
            pgParameter.Direction = ParameterDirection.Output;
            pgCommand.Parameters.Add(pgParameter);


            if (pgCommand.Connection == null)
            {
                pgCommand.Connection = pgConnection;
            }
            OpenConnection();
            //gCommand.ExecuteNonQuery();

            IDataReader dr;
            dr = pgCommand.ExecuteReader();
            while (dr.Read())
            {
                vListado.Add(CargarH<T>(ref dr));
            }

            foreach (var item in parametroSalida)
            {
                (param[item.Key] as ParamOut).Valor = item.Value.Value;
                //Liberamos la variable del diccionario.                
            }

            //Liberamos variables, caso Cursor
            foreach (NpgsqlParameter vItem in pgCommand.Parameters)
            {

            }
            //Liberamos DataReader.
            dr.Dispose();
            //Liberamos cursor.
            //p_rc.Dispose();
            //Liberamos Command.
            pgCommand.Dispose();
            //
            //DisposeConnection();

            return vListado;
        }

        /// <summary>
        /// Devuelve una Dataset de entidades a partir de un procedimiento almacenado
        /// </summary>
        /// <param name="nameProcedure">nombre completo del procedimiento</param>
        /// <param name="param"> parametros de entrada y salida, para los parametros de salia use el objeto ParamOut </param>
        /// <returns></returns>
        public virtual DataSet GetDataSetByProcedureBD(string nameProcedure, params Object[] param)
        {

            pgCommand = new NpgsqlCommand();
            pgCommand.CommandText = nameProcedure;
            pgCommand.Connection = pgConnection;
            pgCommand.CommandText = "declare begin " + Schema + "." + nameProcedure + " (";
            string nomParametro = "parametro";
            int a = 0;
            Dictionary<int, NpgsqlParameter> parametroSalida = new Dictionary<int, NpgsqlParameter>();

            foreach (object item in param)
            {
                pgCommand.CommandText += ":" + nomParametro + a.ToString() + ",";
                NpgsqlParameter parametro = null;

                if (item == null)
                {
                    parametro = new NpgsqlParameter
                    {
                        ParameterName = ":" + nomParametro + a.ToString(),
                        Value = null
                    };
                    a++;
                    pgCommand.Parameters.Add(parametro);
                    continue;
                }

                parametro = new NpgsqlParameter
                {
                    ParameterName = ":" + nomParametro + a.ToString(),
                    Value = item.GetType() == typeof(ParamOut) ? (item as ParamOut).Valor : item,
                    Direction = item.GetType() == typeof(ParamOut) ? ParameterDirection.Output : ParameterDirection.Input
                };
                switch (item.GetType().Name)
                {
                    case "Int32":
                        parametro.NpgsqlDbType = NpgsqlDbType.Integer;
                        break;
                    case "Int64":
                        parametro.NpgsqlDbType = NpgsqlDbType.Integer;
                        break;
                    case "Decimal":
                        parametro.NpgsqlDbType = NpgsqlDbType.Numeric;
                        break;
                    case "String":
                        parametro.NpgsqlDbType = NpgsqlDbType.Varchar;
                        break;
                    case "Boolean":
                        parametro.NpgsqlDbType = NpgsqlDbType.Char;
                        break;
                    case "DateTime":
                        parametro.NpgsqlDbType = NpgsqlDbType.Date;
                        break;
                    case "Byte[]":
                        parametro.NpgsqlDbType = NpgsqlDbType.Bytea;
                        parametro.Value = item.GetType() == typeof(ParamOut) ? (item as ParamOut).Valor : item;
                        break;
                    default:
                        break;
                }
                if (item.GetType() == typeof(ParamOut))
                {
                    parametro.Size = (item as ParamOut).Size;
                    parametroSalida.Add(a, parametro);
                }
                a++;
                pgCommand.Parameters.Add(parametro);

            }
            pgCommand.CommandText += ":cursor_resul); end;";
            var pgParameter = new NpgsqlParameter();

            pgParameter.ParameterName = ":cursor_resul";
            pgParameter.NpgsqlDbType = NpgsqlDbType.Refcursor;
            pgParameter.Direction = ParameterDirection.Output;
            pgCommand.Parameters.Add(pgParameter);

            DataSet vDataSet = new DataSet();
            using (NpgsqlDataAdapter vDataAdapter = new NpgsqlDataAdapter(pgCommand))
            {
                try
                {
                    vDataAdapter.Fill(vDataSet);
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
            foreach (var item in parametroSalida)
            {
                (param[item.Key] as ParamOut).Valor = item.Value.Value;
            }

            //Liberamos variables, caso Cursor
            foreach (NpgsqlParameter vItem in pgCommand.Parameters)
            {

            }
            //Dispose Command
            //gCommand.Cancel();
            //gCommand.Dispose();

            return vDataSet;
        }

        /// <summary>
        /// Ejecuta un procedimiento alamacenado
        /// </summary>
        /// <param name="nameProcedure">nombre completo del procedimiento</param>
        /// <param name="param"> parametros de entrada y salida, para los parametros de salia use el objeto ParamOut </param>
        /// <returns></returns>
        public virtual void CallProcedureBD(string nameProcedure, params Object[] param)
        {

            OpenConnectionForTransaction();

            if (pgTransaction == null) pgTransaction = pgConnectionForTransaction.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);
            if (pgCommandForTransaction == null)
            {
                pgCommandForTransaction = new NpgsqlCommand();
                pgCommandForTransaction.CommandType = CommandType.Text;
                pgCommandForTransaction.Connection = pgConnectionForTransaction;
                pgCommandForTransaction.Transaction = pgTransaction;

            }
            pgCommandForTransaction.CommandText = nameProcedure;

            pgCommandForTransaction.Parameters.Clear();
            pgCommandForTransaction.CommandText = "declare begin " + Schema + "." + nameProcedure + " (";
            string nomParametro = "parametro";
            int a = 0;
            Dictionary<int, NpgsqlParameter> parametroSalida = new Dictionary<int, NpgsqlParameter>();

            foreach (object item in param)
            {
                pgCommandForTransaction.CommandText += ":" + nomParametro + a.ToString() + ",";

                NpgsqlParameter parametro = null;

                if (item == null)
                {
                    parametro = new NpgsqlParameter
                    {
                        ParameterName = ":" + nomParametro + a.ToString(),
                        Value = null
                    };
                    a++;
                    pgCommandForTransaction.Parameters.Add(parametro);
                    continue;
                }
                parametro = new NpgsqlParameter
                {
                    ParameterName = ":" + nomParametro + a.ToString(),
                    Direction = item.GetType() == typeof(ParamOut) ? ParameterDirection.Output : ParameterDirection.Input
                };

                if (item.GetType() == typeof(ParamOut))
                {
                    switch ((item as ParamOut).InOut)
                    {
                        case ParamOut.Type.Out:
                            parametro.Direction = System.Data.ParameterDirection.Output;
                            break;
                        case ParamOut.Type.InOut:
                            parametro.Direction = System.Data.ParameterDirection.InputOutput;
                            break;
                        case ParamOut.Type.In:
                            parametro.Direction = System.Data.ParameterDirection.Input;
                            break;
                        default:
                            parametro.Direction = System.Data.ParameterDirection.Input;
                            break;
                    }
                }
                if (item.GetType() == typeof(ParamOut) && (item as ParamOut).IsObjectOracle)
                {
                    parametro = (item as ParamOut).BindObjecPostGrest(":" + nomParametro + a.ToString());
                }
                switch (item.GetType().Name)
                {
                    case "Int32":
                        parametro.NpgsqlDbType = NpgsqlDbType.Integer;
                        parametro.Value = item.GetType() == typeof(ParamOut) ? (item as ParamOut).Valor : item;
                        break;
                    case "Int64":
                        parametro.NpgsqlDbType = NpgsqlDbType.Integer;
                        parametro.Value = item.GetType() == typeof(ParamOut) ? (item as ParamOut).Valor : item;
                        break;
                    case "Decimal":
                        parametro.NpgsqlDbType = NpgsqlDbType.Numeric;
                        parametro.Value = item.GetType() == typeof(ParamOut) ? (item as ParamOut).Valor : item;
                        break;
                    case "String":
                        parametro.NpgsqlDbType = NpgsqlDbType.Varchar;
                        parametro.Value = item.GetType() == typeof(ParamOut) ? (item as ParamOut).Valor : item;
                        break;
                    case "Boolean":
                        parametro.NpgsqlDbType = NpgsqlDbType.Char;
                        parametro.Value = Convert.ToInt32((item.GetType() == typeof(ParamOut) ? (item as ParamOut).Valor : item));
                        break;
                    case "DateTime":
                        parametro.NpgsqlDbType = NpgsqlDbType.Date;
                        parametro.Value = item.GetType() == typeof(ParamOut) ? (item as ParamOut).Valor : item;
                        break;
                    case "Byte[]":
                        parametro.NpgsqlDbType = NpgsqlDbType.Bytea;
                        parametro.Value = item.GetType() == typeof(ParamOut) ? (item as ParamOut).Valor : item;
                        break;
                    default:
                        break;
                }

                if (item.GetType() == typeof(ParamOut))
                {
                    parametro.Size = (item as ParamOut).Size;
                    parametroSalida.Add(a, parametro);
                }
                a++;
                pgCommandForTransaction.Parameters.Add(parametro);

            }

            pgCommandForTransaction.CommandText = pgCommandForTransaction.CommandText.Substring(0, pgCommandForTransaction.CommandText.Length - 1);//quitamos la ultima coma
            if (pgCommandForTransaction.Parameters.Count == 0)
            {
                pgCommandForTransaction.CommandText += "; end;";
            }
            else
            {
                pgCommandForTransaction.CommandText += "); end;";
            }

            ExecuteNonQueryForTransaction(pgCommandForTransaction);
            foreach (var item in parametroSalida)
            {
                (param[item.Key] as ParamOut).Valor = item.Value.Value;
            }
        }

        #endregion

        #region Constructores

        /// <summary>
        /// Se pasa por referencia la cadena de conexion para casos en los que se quiere compartir una transacción.
        /// </summary>
        /// <param name="pConnection"></param>
        public Repository(ref NpgsqlConnection pConnection)
        {
            try
            {
                pgConnection = pConnection;
                pgConnectionForTransaction = pgConnection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Constructor por defecto para una conexion aislada.
        /// </summary>
        /// <param name="pConnection">Cadena de conexión</param>
        /// <param name="pIsAlias">Bandera que indica si la cadena es un alias para buscar en archivo de configuracion o es la cadena de conexion.</param>
        public Repository(string pConnection, Boolean pIsAlias)
        {
            try
            {
                if (pIsAlias)
                {
                    pgConnection = new NpgsqlConnection(ConfigurationManager.ConnectionStrings[pConnection].ConnectionString);
                    pgConnectionForTransaction = new NpgsqlConnection(ConfigurationManager.ConnectionStrings[pConnection].ConnectionString);
                    ConexionString = "Desde archivo de configuracion: " + ConfigurationManager.ConnectionStrings[pConnection].ConnectionString;
                }
                else
                {
                    pgConnection = new NpgsqlConnection(pConnection);
                    pgConnectionForTransaction = new NpgsqlConnection(pConnection);
                    ConexionString = "Conexion directa: " + pConnection;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Destructor

        public void Dispose()
        {
            try
            {
                DisposeConnection();
                DisposeConnectionForTransaction();

                GC.SuppressFinalize(this);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Destructor.
        /// </summary>
        ~Repository()
        {
            this.Dispose();
        }

        #endregion

        #region Metodos Base

        /// <summary>
        /// Método que setea una lista a una entidad desde un datareader.
        /// </summary>
        /// <typeparam name="T">Tipo Genérico.</typeparam>
        /// <param name="vDataReader">Objeto data reader</param>        
        /// <returns>Lista de eintidad mapeada con datos desde el dataready de oracle</returns>
        protected List<T> _SetListReader<T>(IDataReader vDataReader) where T : Domain.Seedwork.Entity, new()
        {
            List<T> vList = new List<T>();
            try
            {
                while (vDataReader.Read())
                {
                    vList.Add(this.CargarH<T>(ref vDataReader));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return vList;
        }

        /// <summary>
        /// Método que devuelve una determinada entidad.
        /// </summary>
        /// <typeparam name="T">Tipo Genérico.</typeparam>
        /// <param name="pQuery">Consulta SQL.</param>
        /// <param name="parametros">Indican si se cargaran las entidades enlazadas a la entidad.</param>
        /// <returns></returns>
        protected T FindSQL<T>(string pQuery, string[] parametros) where T : Domain.Seedwork.Entity, new()
        {
            IDataReader pvDataReader = null;
            T vEntity = new T();
            try
            {

                PropertyInfo[] propiedades = new PropertyInfo[vEntity.GetType().GetProperties().Count()];
                vEntity.GetType().GetProperties().CopyTo(propiedades, 0);
                propiedades = propiedades.Where(a => !typeof(Entity).GetProperties().Any(b => b.Name == a.Name)).ToArray();

                PropertyInfo[] ForeingKeys = propiedades.Where(a =>
                     a.GetCustomAttributes(typeof(Relation), false).Count() > 0
                     ).ToArray();

                pvDataReader = GetList(pQuery, parametros);

                vEntity = null;
                int i = 0;
                do
                {
                    if (i == 0)
                    {
                        pvDataReader.Read();
                        vEntity = this.CargarH<T>(ref pvDataReader);
                    }
                    else
                    {
                        Type typeChild = ForeingKeys[i - 1].PropertyType.GetGenericArguments()[0];
                        MethodInfo method = this.GetType().GetMethod("_SetListReader");
                        MethodInfo genericMethod = method.MakeGenericMethod(new Type[] { typeChild });
                        ForeingKeys[i - 1].SetValue(vEntity, genericMethod.Invoke(this, new Object[] { pvDataReader }), null);
                    }
                    i++;
                } while (pvDataReader.NextResult());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                DisposeReader(ref pvDataReader);
                DisposeCommand();
                DisposeConnection();
            }
            return vEntity;
        }

        /// <summary>
        /// Devuelve un DataReader con el resultado de la consulta.
        /// </summary>
        /// <param name="pQuery">Consulta SQL.</param>
        /// <returns></returns>
        protected IDataReader GetList(string pQuery, string[] pEntidades)
        {
            if (pEntidades == null)
                pEntidades = new string[1] { "cursor_1" };

            IDataReader vDataReader;

            using (NpgsqlCommand vCommand = new NpgsqlCommand(pQuery, pgConnection))
            {
                foreach (string item in pEntidades)
                {
                    var pgParameter = new NpgsqlParameter();
                    pgParameter.ParameterName = item;
                    pgParameter.NpgsqlDbType = NpgsqlDbType.Refcursor;
                    pgParameter.NpgsqlValue = DBNull.Value;
                    pgParameter.Direction = ParameterDirection.Output;
                    vCommand.Parameters.Add(pgParameter);
                }

                OpenConnection();

                try
                {
                    NpgsqlDataReader Reader = vCommand.ExecuteReader();
                    vDataReader = Reader;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    for (int i = 0; i < vCommand.Parameters.Count; i++)
                    {
                        vCommand.Dispose();
                    }

                }
            }
            return vDataReader;
        }

        #endregion

    }
}
