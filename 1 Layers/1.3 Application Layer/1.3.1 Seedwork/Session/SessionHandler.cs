﻿using Application.Seedwork.Context.Implement;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.SessionState;

namespace Application.Seedwork.Session
{
    public class SessionHandler
    {
        private static volatile SessionHandler gInstance;

        /// <summary>
        /// Constructor privado, para no crear instancias múltiples de la clase
        /// </summary>
        private SessionHandler() { }

        /// <summary>
        /// Permite establecer un acceso directo, para generar una instancia única de acceso a la clase
        /// </summary>
        public static SessionHandler Instance
        {
            get
            {
                if (gInstance == null)
                    gInstance = new SessionHandler();
                return gInstance;
            }
        }

        /// <summary>
        /// Emula una session de estado para seteo de los valores de auditoria con la api key enviada
        /// </summary>
        /// <param name="DatosUsuario">Objecto datos de usuario</param>
        /// <param name="vPasaporte">Pasaporte enviado de usuario</param>
        public void InitContextSession(GestorSession.DatosUsuario DatosUsuario)
        {
            SessionStateUtility.GetHttpSessionStateFromContext(HttpContext.Current)["_ObjDatosUsuario"] = DatosUsuario;
            ContextApplication<GestorSession.DatosUsuario>.Initialize(DatosUsuario);
        }

        /// <summary>
        /// Valida el api key recibido desde el cliente con las credenciales del usuario
        /// </summary>
        /// <param name="request">Objeto reques recibido desde el cliente</param>
        /// <param name="Passport">Nombre del header buscado</param>
        /// <param name="DatosUsuario">Objecto enviado por referencia a ser transformado con los datos del usuario</param>
        /// <param name="vPasaporte">Objecto pasaporte pasado por referencia seteado los valores de usuario</param>
        /// <returns>Falso si no es valido la api key</returns>
        public bool ValidaApiKey(HttpRequestMessage request, string Passport, ref GestorSession.DatosUsuario vPasaporte)
        {
            bool valida = false;
            try
            {
                var strPasport = request.Headers.GetValues(Passport).First();
                DesEncriptar(ref strPasport);
                vPasaporte = JsonConvert.DeserializeObject<GestorSession.DatosUsuario>(strPasport);
                valida = true;

                if (vPasaporte.LOGIN == null)
                {
                    valida = false;
                }

                if (vPasaporte.NOMBRE_USUARIO == null)
                {
                    valida = false;
                }

                if (vPasaporte.COD_FUNCIONARIO == null)
                {
                    valida = false;
                }

            }
            catch (Exception e)
            {
                return false;
            }

            return valida;

        }

        /// <summary>
        /// Funcion que desencripta el apikey recibodo en string
        /// </summary>
        /// <param name="_cadenaAdesencriptar">Cadena api key en base 64</param>
        public static void DesEncriptar(ref string _cadenaAdesencriptar)
        {
            string result = string.Empty;
            byte[] decryted = Convert.FromBase64String(_cadenaAdesencriptar);
            //result = System.Text.Encoding.Unicode.GetString(decryted, 0, decryted.ToArray().Length);
            result = System.Text.Encoding.Unicode.GetString(decryted);
            _cadenaAdesencriptar = result;
        }

    }
}
