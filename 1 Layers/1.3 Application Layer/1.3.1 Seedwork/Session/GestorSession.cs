﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.SessionState;
using System.Web;
using System.Configuration;
using Application.Seedwork.Context;
using System.Globalization;
using System.Runtime.Serialization;

namespace Application.Seedwork.Session
{
    public class GestorSession
    {
        public class DatosUsuario : IPassport
        {
            [DataMember]
            public string LOGIN { get; set; }

            [DataMember]
            public string NOMBRE_USUARIO { get; set; }

            [DataMember]
            public string COD_FUNCIONARIO { get; set; }

            [DataMember]
            public string ID_DEPENDENCIA { get; set; }

            [DataMember]
            public string CARGO { get; set; }

            [DataMember]
            public string COD_REGI { get; set; }

            [DataMember]
            public string DPTO { get; set; }

            [DataMember]
            public string NIVEL_VIATICO { get; set; }

            [DataMember]
            public string ENTIDAD { get; set; }

            [DataMember]
            public string PASS { get; set; }

            CultureInfo IPassport.CultureApplication
            {
                get
                {
                    throw new NotImplementedException();
                }

                set
                {
                    throw new NotImplementedException();
                }
            }
        }

        /// <summary>
        /// Obtiene la url de la pagina actual
        /// </summary>
        public static String CurrrentPage
        {
            get
            {
                string Resul = string.Empty;
                if (HttpContext.Current != null && HttpContext.Current.Request != null)
                {
                    if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["NameVirtualDirectory"]))
                    {
                        Resul = HttpContext.Current.Request.Path;
                    }
                    else
                    {
                        Resul = HttpContext.Current.Request.Path.Replace("/" + ConfigurationManager.AppSettings["NameVirtualDirectory"].ToString(), "");
                    }


                }
                Resul = Resul.Replace("//", "/");
                return Resul;
            }
        }

        /// <summary>
        /// Objeto entidad del usuario
        /// </summary>
        private static DatosUsuario _ObjDatosUsuario;
        public static DatosUsuario ObjDatosUsuario
        {
            get
            {
                HttpContext context = HttpContext.Current;
                if (SessionStateUtility.GetHttpSessionStateFromContext(context)["_ObjDatosUsuario"] == null)
                {
                    _ObjDatosUsuario = new DatosUsuario();
                }
                else
                {
                    _ObjDatosUsuario = (DatosUsuario)SessionStateUtility.GetHttpSessionStateFromContext(context)["_ObjDatosUsuario"];
                }
                return _ObjDatosUsuario;
            }
        }

        /// <summary>
        /// Ingresa un valor en la variable SESSION
        /// </summary>
        /// <param name="key">Llave del valor</param>
        /// <param name="value">Valor a asignar en SESSION</param>
        public static void PutValue(string key, Object value)
        {
            HttpContext context = HttpContext.Current;
            SessionStateUtility.GetHttpSessionStateFromContext(context)[key] = value;
        }

        /// <summary>
        /// Obtiene un valor en la variable SESSION
        /// </summary>
        /// <param name="key">Llave del valor</param>
        /// <returns>retorna el Object correspondiente a la llave</returns>
        public static Object GetValue(string key)
        {
            HttpContext context = HttpContext.Current;
            return SessionStateUtility.GetHttpSessionStateFromContext(context)[key];
        }
    }
}
