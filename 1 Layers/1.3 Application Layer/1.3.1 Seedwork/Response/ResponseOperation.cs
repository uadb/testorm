﻿#region Directivas

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Domain.Seedwork;
using System.Collections;
using Application.Seedwork.Session;
using System.Xml.Serialization;

#endregion

namespace Application.Seedwork.Response
{

    #region Enumeradores

    [DataContract]
    public enum ResponseType
    {
        [EnumMember]
        Success = 1,
        [EnumMember]
        Warning = 2,
        [EnumMember]
        Error = 3,
        [EnumMember]
        NoData = 4
    }

    [DataContract(IsReference = true)]
    [Serializable]
    public class Response
    {
        [DataMember]
        public ResponseType Respuesta { get; set; }
        [DataMember]
        public string Mensaje { get; set; }
        [DataMember]
        public string Auxiliar { get; set; }
    }

    [DataContract(IsReference = true)]
    [Serializable]
    public class ResponseOperation : Response
    {
        [DataMember]
        public IList ListEntities { get; set; }
    }

    [DataContract(IsReference = true)]
    [Serializable]
    [XmlInclude(typeof(IList))]
    public class ResponseQuery : Response 
    {
        [DataMember]        
        public IList ListEntities { get; set; }
    }

    [Serializable]
    [DataContract(IsReference =true)]
    public class ResponseQuery<T> : Response where T : Entity, new()
    {        
        [DataMember]
        public List<T> ListEntities { get; set; }
    }

    [DataContract(IsReference = true)]
    [Serializable]
    public class ResponseEntity : Response
    {
        [DataMember]
        public string Codigo { get; set; }
        [DataMember]
        public Entity ObjectEntity { get; set; }
    }

    [DataContract(IsReference = true)]
    [Serializable]
    public class ResponseEntity<T> : Response where T : Entity, new()
    {
        [DataMember]
        public string Codigo { get; set; }
        [DataMember]
        public T ObjectEntity { get; set; }
    }

    [DataContract(IsReference = true)]
    [Serializable]
    public class ResponseObject<T> : Response
    {
        [DataMember]
        public string Codigo { get; set; }
        [DataMember]
        public T Object { get; set; }
    }

    [DataContract(IsReference = true)]
    [Serializable]
    public class ResponseObject : Response
    {
        [DataMember]
        public string Codigo { get; set; }
        [DataMember]
        public Object Object { get; set; }
    }

    #endregion

}
