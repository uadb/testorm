﻿using System.Runtime.Serialization;
using Domain.Seedwork;
using System;

namespace Application.Seedwork.Configuracion
{

    [Serializable]
    [DataContract]
    public class ConfiguracionWeb : Entity
    {
        #region Propiedades

        [DataMember]
        public string Version { get; set; }

        [DataMember]
        public string NombreSistema { get; set; }

        [DataMember]
        public string SiglaSistema { get; set; }

        [DataMember]
        public string RutaDocumentos { get; set; }

        [DataMember]
        public string ServicioApi {get;set;}

        #endregion

    }
}
