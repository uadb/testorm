﻿using System;
using System.Runtime.Serialization;
using Domain.Seedwork;

namespace Application.Seedwork.Configuracion
{
    [Serializable]
    [DataContract]
    public class ConfiguracionApplication : Entity
    {
        [DataMember]
        public String TypeApplication { get; set; }

        [DataMember]
        public String AssemblyPasaporte { get; set; }

        [DataMember]
        public String TypePassporte { get; set; }

        [DataMember]
        public String nameInitializedSession { get; set; }

        [DataMember]
        public String namePasaporteSession { get; set; }

        [DataMember]
        public String nameContextSession { get; set; }

        [DataMember]
        public String mailAdministrator { get; set; }

        [DataMember]
        public String RutaFormulariosPlantilla { get; set; }

        [DataMember]
        public String RutaFormulariosTmp { get; set; }

        [DataMember]
        public String RutaInstanciasWorkFlow { get; set; }

        [DataMember]
        public Boolean enviarMailAdministrador { get; set; }

        /// <summary>
        /// Parámetro que obtiene y/o establece la Contraseña del Archivo Excel
        /// </summary>
        [DataMember]
        public string ContraseñaArchivosExcel { get; set; }

    }
}