﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace Application.Seedwork.Context
{
    public interface IPassport
    {
        [System.Xml.Serialization.XmlIgnoreAttribute]
        CultureInfo CultureApplication { get; set; }
    }
}
