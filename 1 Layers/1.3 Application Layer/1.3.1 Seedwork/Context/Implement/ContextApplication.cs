﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.IO;

using System.Web;
using System.Configuration;
using Application.Seedwork.Session;
using System.Reflection;
using System.Web.Hosting;
using System.Threading;
using Application.Seedwork.Configuracion;
using Application.Seedwork.File;
using Application.Seedwork.Utiles;

namespace Application.Seedwork.Context.Implement
{
    /// <summary>
    /// Implementacion del contrato de contexto.
    /// Permite obtener datos genericos del aplicativo como el pasaporte y la gestion
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [Serializable]
    public class ContextApplication<T> where T : GestorSession.DatosUsuario, new()
    {
        private static string vRuta = "";
        private static ConfiguracionApplication vConfiguracion;
        private static ContextApplication<T> Instance;
        private static bool IsInitialized = false;

        private static ThreadLocal<bool> IsInitialized_WebServices = new ThreadLocal<bool> (() => true);
        public static bool IsInitialized_WebServices_Get
        {
            get { return IsInitialized_WebServices.Value; }
            set
            {
                IsInitialized_WebServices.Value = value;
            }
        }

        private const string nameConfigObjectSession = "nameConfigObjectSession";


        private static ThreadLocal<ContextApplication<T>> Instance_WebServices = new ThreadLocal<ContextApplication<T>>(() => new ContextApplication<T>());

        private static ContextApplication<T> Instance_WebServicesGet
        {
            get { return Instance_WebServices.Value; }
        }
        public static string Gestion
        {
            get
            {
                return DateTime.Now.Year.ToString();
            }
        }

        private T _passport;
        /// <summary>
        /// Pasaporte con el que el usuario ingreso al sistema
        /// </summary>
        public T Passport
        {
            get
            {
                return _passport;
            }
        }

        private ConfiguracionApplication _CustomConfigurationApplication;
        public ConfiguracionApplication CustomConfigurationApplication
        {
            get
            {
                return _CustomConfigurationApplication;
            }
        }

        /// <summary>
        /// Inicializael contexto de la aplicacion
        /// </summary>
        /// <param name="pPassaport"></param>
        public static void Initialize(T pPassaport)
        {

            Instance = new ContextApplication<T>();
            vConfiguracion = BindFileConfig();

            switch (vConfiguracion.TypeApplication)
            {
                case "Web":
                    if (GestorSession.GetValue(vConfiguracion.nameInitializedSession) == null)
                    {
                        Instance._passport = pPassaport;
                        IsInitialized = true;
                        GestorSession.PutValue(vConfiguracion.nameInitializedSession, IsInitialized);
                        GestorSession.PutValue(vConfiguracion.nameContextSession, Instance);
                    }
                    else
                    {
                        Instance = GestorSession.GetValue(vConfiguracion.nameContextSession) as ContextApplication<T>;
                    }

                    break;
                case "Test":
                    Instance._passport = pPassaport;
                    IsInitialized = true;
                    vConfiguracion = BindFileConfig();
                    break;
                case "Desktop":
                    Instance._passport = pPassaport;
                    IsInitialized = true;
                    break;
                case "Windows":
                    Instance._passport = pPassaport;
                    IsInitialized = true;
                    break;
                case "WebServices":
                    IsInitialized_WebServices_Get = true;
                    Instance_WebServicesGet._passport = pPassaport;
                    vConfiguracion = BindFileConfig();
                    break;
                case "WebApi":
                    IsInitialized_WebServices_Get = true;
                    Instance_WebServicesGet._passport = pPassaport;
                    vConfiguracion = BindFileConfig();
                    break;
                default:
                    break;
            }

        }

        /// <summary>
        /// Obtiene el objeto de configuracion a partir del archivo de configuracion
        /// </summary>
        /// <returns></returns>
        private static ConfiguracionApplication BindFileConfig(bool pInitialized = false)
        {
            ConfiguracionApplication vConfiguracion = new ConfiguracionApplication();

            if (ConfigurationManager.AppSettings["TypeApplication"].ToString().Equals("Web"))
            {
                vRuta = HttpContext.Current.Server.MapPath("~/ConfiguracionApplication.config");
            }
            else if (ConfigurationManager.AppSettings["TypeApplication"].ToString().Equals("WebServices"))
            {
                vRuta = HostingEnvironment.MapPath("~/ConfiguracionApplication.config");
            }
            else if (ConfigurationManager.AppSettings["TypeApplication"].ToString().Equals("Test"))
            {
                vRuta = Path.GetDirectoryName(Assembly.GetAssembly(typeof(ConfiguracionApplication)).CodeBase) + "\\ConfiguracionApplication.config";
                vRuta = vRuta.Replace("file:\\", "");
            }
            else if (ConfigurationManager.AppSettings["TypeApplication"].ToString().Equals("WebApi"))
            {
                vRuta = HttpContext.Current.Server.MapPath("~/Web.config");
            }
            else
            {
                vRuta = Directory.GetParent(Assembly.GetEntryAssembly().Location).FullName + "\\ConfiguracionApplication.config";
            }

            FileUtil vFileUtil = new FileUtil(vRuta, false);

            if (vFileUtil.existFile() && !ConfigurationManager.AppSettings["TypeApplication"].ToString().Equals("WebApi"))
            {
                vConfiguracion = Serializer.DeserializarTo<ConfiguracionApplication>(vFileUtil.GetData());

            } else if (ConfigurationManager.AppSettings["TypeApplication"].ToString().Equals("WebApi")) {

                vConfiguracion.TypeApplication = ConfigurationManager.AppSettings["TypeApplication"].ToString();
                vConfiguracion.namePasaporteSession = ConfigurationManager.AppSettings["namePasaporteSession"].ToString();
                vConfiguracion.nameInitializedSession = ConfigurationManager.AppSettings["nameInitializedSession"].ToString();
                vConfiguracion.nameContextSession = ConfigurationManager.AppSettings["nameContextSession"].ToString();
            }
            else
            {
                throw new Exception("Archivo de Configuracion de aplicacion no encontrado : " + vRuta);
            }



            if (ConfigurationManager.AppSettings["TypeApplication"].ToString().Equals("Web"))
                if (!pInitialized)
                {
                    Instance._CustomConfigurationApplication = vConfiguracion;
                }
            if (ConfigurationManager.AppSettings["TypeApplication"].ToString().Equals("WebServices"))
                if (!IsInitialized_WebServices_Get)
                {
                    Instance_WebServicesGet._CustomConfigurationApplication = vConfiguracion;
                }



            return vConfiguracion;
        }

        /// <summary>
        /// Obtiene la instancia de la configuracion de la aplicacion
        /// </summary>
        /// <returns></returns>
        public static ContextApplication<T> GetInstance()
        {
            if (ConfigurationManager.AppSettings["TypeApplication"].ToString().Equals("Web") && vConfiguracion != null)
            {
                IsInitialized = GestorSession.GetValue(vConfiguracion.nameInitializedSession) == null ? false : true;
                Instance = GestorSession.GetValue(vConfiguracion.nameContextSession) as ContextApplication<T>;
                return Instance;
            }
            else if (ConfigurationManager.AppSettings["TypeApplication"].ToString().Equals("WebServices") && vConfiguracion != null)
            {
                return Instance_WebServicesGet;
            }
            else if (ConfigurationManager.AppSettings["TypeApplication"].ToString().Equals("WebApi") && vConfiguracion != null)
            {
                return Instance_WebServicesGet;
            }
            else if(ConfigurationManager.AppSettings["TypeApplication"].ToString().Equals("Test"))
            {
                return Instance;
            }
            return null;
        }
    }
}
