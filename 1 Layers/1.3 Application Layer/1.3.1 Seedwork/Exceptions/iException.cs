﻿#region Directivas

using System;
using System.Runtime.Serialization;

#endregion

namespace Application.Seedwork.Exceptions
{
    public class iException : Exception
    {

        #region Variables

        private Application.Seedwork.Enumerators.CodeExceptionAplication _Codigo;

        #endregion

        public Application.Seedwork.Enumerators.CodeExceptionAplication Codigo { get { return _Codigo; } }


        #region Constructores

        /// <summary>
        /// Contructor que rescata la excepcion generada.
        /// </summary>
        /// <param name="innerException"></param>
        public iException(System.Exception innerException)
            : base("iExcepcion", innerException)
        {
            if (innerException.Source == null)
            {
                _Codigo = Enumerators.CodeExceptionAplication.ErrorGenerico;
            }
            else if (innerException.Source.Contains("Infrastructure"))
            {
                _Codigo = Enumerators.CodeExceptionAplication.ErrorInfraestructura;
            }
            else if (innerException.Source.Contains("Domain"))
            {
                _Codigo = Enumerators.CodeExceptionAplication.ErrorDominio;
            }
            else if (innerException.Source.Contains("Application"))
            {
                _Codigo = Enumerators.CodeExceptionAplication.ErrorAplicacion;
            }
            else if (innerException.Source.Contains("Presentation"))
            {
                _Codigo = Enumerators.CodeExceptionAplication.ErrorPresentacion;
            }

        }

        /// <summary>
        /// Constructor que rescata ademas de la excepcion y el codigo.
        /// </summary>
        /// <param name="innerException"></param>
        /// <param name="codigoExcepcion"></param>
        public iException(Exception innerException, Application.Seedwork.Enumerators.CodeExceptionAplication codigoExcepcion)
            : base("iExcepcion", innerException)
        {
            _Codigo = codigoExcepcion;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public iException()
        {

        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="message"></param>
        public iException(string message)
            : base(message)
        {

        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innerException"></param>
        public iException(string message, Exception innerException)
            : base(message, innerException)
        {

        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected iException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {

        }

        #endregion

    }
}