﻿using System;
using System.Collections.Generic;
using System.Linq;
using Application.Seedwork.Resources;
using System.Reflection;
using Infrastructure.Crosscutting.Utiles;

namespace Application.Seedwork.Exceptions
{
    public abstract class iExceptionManager
    {
        private static string _MsgError;
        public static string MsgError { get { return _MsgError; } }
        public static string CodeError { get { return "Codigo de Error"; } }

        #region Metodos

        /// <summary>
        /// Metodo para procesar un error ISAP.
        /// </summary>
        /// <param name="excepcion"></param>
        public static void ProcessError(iException excepcion)
        {
            string trace = "";
            string nameTrace = "";
            nameTrace = "Error";

            if (excepcion.Codigo == Enumerators.CodeExceptionAplication.ErrorGenerico)
            {
                trace = Errors.ErrorLayerGral + excepcion.InnerException.Message;
            }
            else if (excepcion.Codigo == Enumerators.CodeExceptionAplication.ErrorInfraestructura)
            {
                trace = Errors.ErroLayerInfrastructure + excepcion.InnerException.Message;
            }
            else if (excepcion.Codigo == Enumerators.CodeExceptionAplication.ErrorDominio)
            {
                trace = Errors.ErroLayerDomain + excepcion.InnerException.Message;
            }
            else if (excepcion.Codigo == Enumerators.CodeExceptionAplication.ErrorAplicacion)
            {
                trace = Errors.ErroLayerApplication + excepcion.InnerException.Message;
            }
            else if (excepcion.Codigo == Enumerators.CodeExceptionAplication.ErrorPresentacion)
            {
                trace = Errors.ErroLayerPresentacion + excepcion.InnerException.Message;
            }
            else
            {
                trace = excepcion.InnerException != null ? excepcion.InnerException.Message : (excepcion as Exception).Message;
            }

            trace += excepcion.InnerException != null ? excepcion.InnerException.StackTrace : (excepcion as Exception).StackTrace;
            
            if (excepcion.InnerException != null && excepcion.InnerException.InnerException != null)
            {
                trace += " .Error heredado" + excepcion.InnerException.InnerException.Message + ". " + excepcion.InnerException.InnerException.StackTrace;
            }
           
            String ticket = String.Format(Errors.Ticket, (new CustomGuid()).GetGuid());

            //Nivel archivo NLog
            Bitacora.RegisterNLog(String.Format("{0}{1}", ticket, trace));

            //Nivel archivo SO
            Bitacora.RegisterTraceSO(nameTrace, String.Format("{0}{1}", ticket, trace));

            string mensajeExcepcion = excepcion.Message + (excepcion.InnerException != null ? excepcion.InnerException.Message : string.Empty);

            List<PropertyInfo> lPropiedades = typeof(Errors).GetProperties().ToList().Where(x => x.GetValue(null, null) != null && mensajeExcepcion.Contains(x.Name.Replace("_", "-"))).ToList();
            if (lPropiedades.Count > 0)
            {            
                _MsgError = Errors.ErroLayerApplication + "verifique la bitácora con el administrador " + ticket;
            }
            else
            {
                _MsgError = Errors.ErrorGral + ticket;
            }
        }

        #endregion

    }
}
