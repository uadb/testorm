﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Application.Seedwork.Enumerators
{
    public enum CodeExceptionAplication
    {
        ErrorInfraestructura = 500,
        ErrorAplicacion = 700,
        ErrorDominio = 900,
        ErrorPresentacion = 1100,
        ErrorGenerico = 1000,
    }

    public enum MessageType
    {
        Error = 1,
        Message = 2,
        Warning = 3 ,
        Debug = 4
    }

}
