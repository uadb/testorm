﻿using Application.Seedwork.Context.Implement;
using Application.Seedwork.Session;
using Domain.Seedwork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Application.Seedwork.Utiles
{
    public class HelperEntity
    {

        /// <summary>
        /// Asigna las propiedades a las entidades POCO 
        /// </summary>
        /// <param name="entidad">Entidad a asignar propiedadees de auditoria</param>
        public static void setPropiedaesAuditoria(Entity entidad)
        {
            if (entidad.State == Domain.Seedwork.Enumerators.State.Insert || entidad.State == Domain.Seedwork.Enumerators.State.None)
            {
                entidad.GetType().GetProperty("FECHA_CREACION").SetValue(entidad, DateTime.Now, null);
                entidad.GetType().GetProperty("USUARIO_CREACION").SetValue(entidad, ContextApplication<GestorSession.DatosUsuario>.GetInstance().Passport.LOGIN, null);

            }
            else if (entidad.State == Domain.Seedwork.Enumerators.State.Update)
            {
                entidad.GetType().GetProperty("FECHA_MODIFICACION").SetValue(entidad, DateTime.Now, null);
                entidad.GetType().GetProperty("USUARIO_MODIFICACION").SetValue(entidad, ContextApplication<GestorSession.DatosUsuario>.GetInstance().Passport.LOGIN, null);
            }
            else if (entidad.State == Domain.Seedwork.Enumerators.State.Delete) {

                entidad.GetType().GetProperty("FECHA_ELIMINACION").SetValue(entidad, DateTime.Now, null);
                entidad.GetType().GetProperty("USUARIO_ELIMINACION").SetValue(entidad, ContextApplication<GestorSession.DatosUsuario>.GetInstance().Passport.LOGIN, null);
                entidad.State = Domain.Seedwork.Enumerators.State.Update;
            }
        }

    }
}
