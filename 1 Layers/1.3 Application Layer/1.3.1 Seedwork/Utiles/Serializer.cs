﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Application.Seedwork.Utiles
{
    public static class Serializer
    {
        public static string SerializarToXml(this object obj)
        {
            try
            {
                StringWriter strWriter = new StringWriter();
                XmlSerializer serializer = new XmlSerializer(obj.GetType());

                serializer.Serialize(strWriter, obj);
                string resultXml = strWriter.ToString();
                strWriter.Close();

                return resultXml;
            }
            catch
            {
                return string.Empty;
            }
        }


        public static T DeserializarTo<T>(this string xmlSerializado)
        {
            try
            {
                XmlSerializer xmlSerz = new XmlSerializer(typeof(T));

                using (StringReader strReader = new StringReader(xmlSerializado))
                {
                    object obj = xmlSerz.Deserialize(strReader);
                    return (T)obj;
                }
            }
            catch { return default(T); }
        }


        public static byte[] SerializarBinario(object obj)
        {
            if (obj == null) return null;

            MemoryStream stream = new MemoryStream();
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(stream, obj);

            stream.Seek(0, 0);
            byte[] result = stream.ToArray();

            stream.Close();

            return result;
        }

        public static object DeserializarBinario(byte[] data)
        {
            if (data == null || data.Length == 0) return null;

            BinaryFormatter binaryFormatter = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            stream.Write(data, 0, data.Length);
            stream.Flush();
            stream.Seek(0, 0);
            return binaryFormatter.Deserialize(stream);
        }
    }
}
