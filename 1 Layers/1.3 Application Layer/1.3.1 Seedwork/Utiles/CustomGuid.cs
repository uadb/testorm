﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Crosscutting.Utiles
{
    public class CustomGuid
    {
        private static System.Guid GUID
        {
            get
            {
                return System.Guid.NewGuid();
            }
        }

        public String GetGuid()
        {
            byte[] bArr = GUID.ToByteArray();
            int autonum = BitConverter.ToInt32(bArr, 0);
            autonum = Math.Abs(autonum) + DateTime.Now.Minute + DateTime.Now.Second + DateTime.Now.Millisecond;
            String _sGUID = autonum.ToString();
            return _sGUID;
        }
    }
}
