﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;
using System.Diagnostics;
using System.Configuration;

namespace Application.Seedwork
{
    public class Bitacora
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Metodo para registar el trace de un evento en un archivo plano nLog
        /// el registro se realiza en un archivo distinto por dia.
        /// </summary>
        public static void RegisterNLog(String trace)
        {
            logger.Log(LogLevel.Info, trace);
        }

        /// <summary>
        /// Metodo para registrar una traza en la bitacora del sistema operativo.
        /// </summary>
        /// <param name="trace"></param>
        public static void RegisterTraceSO(String NameTrace, String trace)
        {
            string LogConfigName = ConfigurationManager.AppSettings["LogConfigName"];
            string LogConfigDescription = ConfigurationManager.AppSettings["LogConfigDescription"];


            if (!EventLog.SourceExists(LogConfigName))
            {
                EventLog.CreateEventSource(LogConfigName, LogConfigDescription);
            }
            EventLog myLog = new EventLog();
            myLog.Source = LogConfigName;
            if (NameTrace.Equals("Debug"))
            {
                myLog.WriteEntry(trace, EventLogEntryType.Warning);    
            }
            else if (NameTrace.Equals("Error"))
            {
                myLog.WriteEntry(trace, EventLogEntryType.Error);    
            }
            else
            {
                myLog.WriteEntry(trace, EventLogEntryType.Information);    
            }
            
        }
    }
}
