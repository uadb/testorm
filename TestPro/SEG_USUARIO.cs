﻿using Domain.Seedwork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TestPro
{

    [Serializable]
    [DataContract]
    public partial class SEG_USUARIO : Entity
    {
        [Key]
        [DataMember]
        public decimal IDSEG_USUARIO { get; set; }
        [DataMember]
        public decimal IDSEG_UNIDADDEPENDIENTE { get; set; }
        [DataMember]
        public decimal IDSEG_PERFIL { get; set; }
        [DataMember]
        public bool ESTADO { get; set; }
        [DataMember]
        public string NRO_CI { get; set; }
        [DataMember]
        public string LOGIN_USUARIO { get; set; }
        [DataMember]
        public string PASSWORD { get; set; }
        [DataMember]
        public string USUARIO_CREACION { get; set; }
        [DataMember]
        public DateTime FECHA_CREACION { get; set; }
        [DataMember]
        public string USUARIO_MODIFICACION { get; set; }
        [DataMember]
        public DateTime FECHA_MODIFICACION { get; set; }

    }
}
