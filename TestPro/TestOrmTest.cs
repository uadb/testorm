﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Application.Seedwork.Response;
using Domain.Seedwork.Expression;
using System;

namespace TestPro
{
    /// <summary>
    ///This is a test class for TestOrmTest and is intended
    ///to contain all TestOrmTest Unit Tests
    ///</summary>
    [TestClass()]
    public class TestOrmTest
    {

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// Debe Insertar y confirmar el cambio en la base de datos.
        /// </summary>
        [TestMethod()]
        public void _SaveChangInsertCommitTest()
        {
            ResponseOperation vRes = new ResponseOperation();
            SEG_USUARIO pUsuario = new SEG_USUARIO();
            string sG = Guid.NewGuid().ToString();

            pUsuario.IDSEG_UNIDADDEPENDIENTE = 1;
            pUsuario.IDSEG_PERFIL = 1;
            pUsuario.ESTADO = true;
            pUsuario.NRO_CI = sG;
            pUsuario.LOGIN_USUARIO = sG;
            pUsuario.PASSWORD = sG;
            pUsuario.USUARIO_CREACION = "nviscarra";
            pUsuario.FECHA_CREACION = DateTime.Now;
            pUsuario.State = Domain.Seedwork.Enumerators.State.Insert;

            CGeneric.CtxLocal.SaveChanges(pUsuario);
            CGeneric.CtxLocal.CommitChanges();
            vRes.Respuesta = pUsuario.IDSEG_USUARIO != 0 ? ResponseType.Success : ResponseType.Error;
            Assert.AreEqual<ResponseType>(ResponseType.Success, vRes.Respuesta);
        }

        /// <summary>
        /// Debe Actualizar el registro y confirmar el cambio en la base de datos.
        /// </summary>
        [TestMethod()]
        public void _SaveChangUpdateTest()
        {
            
            ResponseOperation vRes = new ResponseOperation();
            SEG_USUARIO pUsuario = new SEG_USUARIO();

            pUsuario = CGeneric.CtxLocal.GetByID<SEG_USUARIO>(217);

            string sG = Guid.NewGuid().ToString();
            
            pUsuario.ESTADO = pUsuario.ESTADO == false;
            pUsuario.NRO_CI = sG;
            pUsuario.USUARIO_MODIFICACION = "nviscarra";
            pUsuario.FECHA_MODIFICACION = DateTime.Now;
            pUsuario.State = Domain.Seedwork.Enumerators.State.Update;

            CGeneric.CtxLocal.SaveChanges(pUsuario);
            CGeneric.CtxLocal.CommitChanges();

            vRes.Respuesta = pUsuario.IDSEG_USUARIO != 0 ? ResponseType.Success : ResponseType.Error;
            Assert.AreEqual<ResponseType>(ResponseType.Success, vRes.Respuesta);
            
        }

        /// <summary>
        /// Debe Intentar Insertar un registro y cancelar el cambio en la base de datos
        /// </summary>
        [TestMethod()]
        public void _SaveChangeInsertRollbackTest()
        {

            ResponseOperation vRes = new ResponseOperation();
            SEG_USUARIO pUsuario = new SEG_USUARIO();
            string sG = Guid.NewGuid().ToString();

            pUsuario.IDSEG_UNIDADDEPENDIENTE = 1;
            pUsuario.IDSEG_PERFIL = 1;
            pUsuario.ESTADO = true;
            pUsuario.NRO_CI = sG;
            pUsuario.LOGIN_USUARIO = sG;
            pUsuario.PASSWORD = sG;
            pUsuario.USUARIO_CREACION = "nviscarra";
            pUsuario.FECHA_CREACION = DateTime.Now;
            pUsuario.State = Domain.Seedwork.Enumerators.State.Insert;

            CGeneric.CtxLocal.SaveChanges(pUsuario);            
            CGeneric.CtxLocal.RollbackChanges();

            vRes.Respuesta = pUsuario.IDSEG_USUARIO != 0 ? ResponseType.Success : ResponseType.Error;
            
            if(vRes.Respuesta == ResponseType.Error)
                Assert.AreEqual<ResponseType>(ResponseType.Success, vRes.Respuesta);

            SEG_USUARIO pUsuarioVerificar = new SEG_USUARIO();
            pUsuarioVerificar = CGeneric.CtxLocal.GetByID<SEG_USUARIO>(pUsuario.IDSEG_USUARIO);

            vRes.Respuesta = pUsuarioVerificar != null ? ResponseType.Error: ResponseType.Success;
            Assert.AreEqual<ResponseType>(ResponseType.Success, vRes.Respuesta);

        }


        /// <summary>
        /// Realizar una búsqueda de registros por el operador Where y Like         
        /// </summary>
        [TestMethod()]
        public void _WhereLikeTest()
        {
            ResponseQuery vRes = new ResponseQuery();
            vRes.ListEntities = CGeneric.Ctx.Where<SEG_USUARIO>(x => ExpressionEntity.Like<string>(y => x.LOGIN_USUARIO, "%RQU%"));
            vRes.Respuesta = vRes.ListEntities.Count > 0 ? ResponseType.Success : ResponseType.Error;
            Assert.AreEqual<ResponseType>(ResponseType.Success, vRes.Respuesta);
        }

        /// <summary>
        /// Realizar una búsqueda de registro por el operador Where y 
        /// buscar por el operador Equal como parámetro un booleano
        /// </summary>
        [TestMethod()]
        public void _WhereBoolTest()
        {
            ResponseQuery vRes = new ResponseQuery();
            vRes.ListEntities = CGeneric.CtxLocal.Where<SEG_USUARIO>(x => ExpressionEntity.Equal<bool>(y => x.ESTADO, true));
            vRes.Respuesta = vRes.ListEntities.Count > 0 ? ResponseType.Success : ResponseType.Error;            
            Assert.AreEqual<ResponseType>(ResponseType.Success, vRes.Respuesta);
        }

        /// <summary>
        ///Obtiene un registro por un id o llave primaria
        ///</summary>
        [TestMethod()]
        public void _GetByIdTest()
        {
            ResponseEntity vRes = new ResponseEntity();
            vRes.ObjectEntity = CGeneric.Ctx.GetByID<SEG_USUARIO>(169);
            vRes.Respuesta = vRes.ObjectEntity != null ? ResponseType.Success : ResponseType.Error;
            Assert.AreEqual<ResponseType>(ResponseType.Success, vRes.Respuesta);
        }

        /// <summary>
        ///Obtener todos los registros de la base de datos
        ///</summary>
        [TestMethod()]
        public void _GetAllTest()
        {
            ResponseQuery vRes = new ResponseQuery();
            vRes.ListEntities = CGeneric.Ctx.GetAll<SEG_USUARIO>();
            vRes.Respuesta = vRes.ListEntities.Count > 0 ? ResponseType.Success : ResponseType.Error;            
            Assert.AreEqual<ResponseType>(ResponseType.Success, vRes.Respuesta);
        }


    }
}
