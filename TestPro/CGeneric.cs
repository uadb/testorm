﻿using Domain.Seedwork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestPro
{
    public class CGeneric
    {

        #region ContexSoat
        /// <summary>
        /// Esteblecer y/o reutilizar conexión desde webconfig
        /// </summary>
        private static Repository<Entity> gCtx;
        public static Repository<Entity> Ctx
        {
            get
            {
                if (gCtx != null)
                {
                    return gCtx;
                }
                else
                {
                    gCtx = new Repository<Entity>("POST", true);
                    return gCtx;
                }
            }
        }
        #endregion 


        #region ContexSoat
        /// <summary>
        /// Esteblecer y/o reutilizar conexión desde webconfig
        /// </summary>
        private static Repository<Entity> gCtxLocal;
        public static Repository<Entity> CtxLocal
        {
            get
            {
                if (gCtxLocal != null)
                {
                    return gCtxLocal;
                }
                else
                {
                    gCtxLocal = new Repository<Entity>("LOCAL", true);
                    return gCtxLocal;
                }
            }
        }
        #endregion 


    }
}
